<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';

class widgetsDemo_Controller extends bab_Controller
{
	protected function getControllerTg()
	{
		return 'addon/widgetsDemo/main';
	}


	/**
	 * Get object name to use in URL from the controller classname
	 * @param string $classname
	 * @return string
	 */
	protected function getObjectName($classname)
	{
		$prefix = strlen('widgetsDemo_Ctrl');
		return strtolower(substr($classname, $prefix));
	}



	public function codeSection($methodNames)
	{
		$W = bab_Widgets();

		if (!is_array($methodNames)) {
			$methodNames = array($methodNames);
		}

		$html = "<?php\n\n/* ... */\n\n";

		foreach ($methodNames as $methodName) {
			$html .= widgetsDemo_methodCode($methodName) . "\n";
		}
		$html .= "\n/* ... */\n\n?>";

		$section = $W->VBoxItems(
			$W->Link(widgetsDemo_translate('Code'), '')->addClass('icon', Func_Icons::ACTIONS_HELP, 'widget-instant-button'),
			$W->Html(highlight_string($html, true))->addClass('widget-instant-form')
		)->addClass(Func_Icons::ICON_LEFT_16, 'widget-instant-container');

		return $section;
	}


	/**
	 *
	 * @param string|Widget_Label $label
	 * @param Widget_Displayable_Interface $item
	 * @return Widget_VBoxLayout
	 */
	protected function labelledItem($label, Widget_Displayable_Interface $item)
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget($label, $item);
	}




	/**
	 * @return widgetsDemo_CtrlMain
	 */
	public function Main($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/main.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlMain', $proxy);
	}


	/**
	 * @return widgetsDemo_CtrlEvent
	 */
	public function Event($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/event.ctrl.php';
	    return bab_Controller::ControllerProxy('widgetsDemo_CtrlEvent', $proxy);
	}



// 	/**
// 	 * @return widgetsDemo_CtrlDemo
// 	 */
// 	public function Demo($proxy = true)
// 	{
// 		require_once dirname(__FILE__) . '/ctrl/demo.ctrl.php';
// 		return bab_Controller::ControllerProxy('widgetsDemo_CtrlDemo', $proxy);
// 	}


	/**
	 * @return widgetsDemo_CtrlInputDemo
	 */
	public function InputDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/inputdemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlInputDemo', $proxy);
	}


	/**
	 * @return widgetsDemo_CtrlDatePickerDemo
	 */
	public function DatePickerDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/datepickerdemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlDatePickerDemo', $proxy);
	}


	/**
	 * @return widgetsDemo_CtrlRadioMenuDemo
	 */
	public function RadioMenuDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/radiomenudemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlRadioMenuDemo', $proxy);
	}


	/**
	 * @return widgetsDemo_CtrlMapDemo
	 */
	public function MapDemo($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/mapdemo.ctrl.php';
	    return bab_Controller::ControllerProxy('widgetsDemo_CtrlMapDemo', $proxy);
	}

	/**
	 * @return widgetsDemo_CtrlAccordionDemo
	 */
	public function AccordionDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/accordiondemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlAccordionDemo', $proxy);
	}



	/**
	 * @return widgetsDemo_CtrlFormDemo
	 */
	public function FormDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/formdemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlFormDemo', $proxy);
	}



	/**
	 * @return widgetsDemo_CtrlTreeviewDemo
	 */
	public function TreeviewDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/treeviewdemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlTreeviewDemo', $proxy);
	}




	/**
	 * @return widgetsDemo_CtrlOrgChartDemo
	 */
	public function OrgChartDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/orgchartdemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlOrgChartDemo', $proxy);
	}




	/**
	 * @return widgetsDemo_CtrlCalendarDemo
	 */
	public function CalendarDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/calendardemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlCalendarDemo', $proxy);
	}




	/**
	 * @return widgetsDemo_CtrlTimelineDemo
	 */
	public function TimelineDemo($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/timelinedemo.ctrl.php';
	    return bab_Controller::ControllerProxy('widgetsDemo_CtrlTimelineDemo', $proxy);
	}




	/**
	 * @return widgetsDemo_CtrlTableViewDemo
	 */
	public function TableViewDemo($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/tableviewdemo.ctrl.php';
	    return bab_Controller::ControllerProxy('widgetsDemo_CtrlTableViewDemo', $proxy);
	}



	/**
	 * @return widgetsDemo_CtrlTableModelViewDemo
	 */
	public function TableModelViewDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/tablemodelviewdemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlTableModelViewDemo', $proxy);
	}

	/**
	 * @return widgetsDemo_CtrlPopupMenuDemo
	 */
	public function PopupMenuDemo($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/popupmenudemo.ctrl.php';
	    return bab_Controller::ControllerProxy('widgetsDemo_CtrlPopupMenuDemo', $proxy);
	}

	/**
	 * @return widgetsDemo_CtrlPortletDemo
	 */
	public function PortletDemo($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/portletdemo.ctrl.php';
	    return bab_Controller::ControllerProxy('widgetsDemo_CtrlPortletDemo', $proxy);
	}


	/**
	 * @return widgetsDemo_CtrlAuthenticationDemo
	 */
	public function AuthenticationDemo($proxy = true)
	{
		require_once dirname(__FILE__) . '/ctrl/authenticationdemo.ctrl.php';
		return bab_Controller::ControllerProxy('widgetsDemo_CtrlAuthenticationDemo', $proxy);
	}


	/**
	 * @return widgetsDemo_CtrlNotificationDemo
	 */
	public function NotificationDemo($proxy = true)
	{
	    require_once dirname(__FILE__) . '/ctrl/notificationdemo.ctrl.php';
	    return bab_Controller::ControllerProxy('widgetsDemo_CtrlNotificationDemo', $proxy);
	}}
