;<?php/*

[general]
name="widgetsDemo"
version="0.1.1"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description="Demo of UI Widgets"
delete=1
ov_version="6.6.90"
php_version="5.1.0"
addon_access_control="1"
preinstall_script="preinstall.php"
author="Laurent Choulette (laurent.choulette@cantico.fr)"
icon="tools-wizard.png"

[addons]
widgets="1.0.103"
LibOrm="0.9.0"

[functionalities]
jquery="Available"
Icons="Available"

*/?>