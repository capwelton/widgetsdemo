<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';

widgetsDemo_loadOrm();



/**
 * Samples.
 *
 * @property ORM_PkField            $id
 * @property ORM_StringField        $line
 * @property ORM_BoolField          $checkbox
 * @property ORM_FileField          $file
 */
class widgetsDemo_FormDemoSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('line')
                ->setDescription(widgetsDemo_translate('Line')),
            ORM_BoolField('checkbox')
                ->setDescription(widgetsDemo_translate('Checkbox')),
            ORM_FileField('file1')
                ->setDescription(widgetsDemo_translate('File 1')),
            ORM_FileField('file2')
                ->setDescription(widgetsDemo_translate('File 2'))
        );
    }
}


/**
 * Samples.
 *
 * @property int           $id
 * @property string        $line
 * @property bool          $checkbox
 * @property bab_Path      $file
 */
class widgetsDemo_FormDemo extends ORM_MySqlRecord
{
}
