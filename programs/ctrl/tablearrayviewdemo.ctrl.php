<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';

require_once dirname(__FILE__) . '/../set/sample.class.php';


/**
 *
 */
class widgetsDemo_CtrlTableArrayViewDemo extends widgetsDemo_Controller
{




    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Sample table array view'),
                $W->VBoxItems(
                    $this->tablearrayviewDemo1()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem(
            $this->codeSection(
                array(
                    'widgetsDemo_CtrlTableArrayViewDemo::tablearrayview',
                    'widgetsDemo_CtrlTableArrayViewDemo::tablearrayviewDemo1'
                )
            )
        );

        /*$box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Table array views'),
                $W->VBoxItems(
                    $this->tablearrayviewDemo2()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem(
               $this->codeSection(
                   array(
                       'widgetsDemo_CtrlTableArrayViewDemo::tablearrayview',
                       'widgetsDemo_CtrlTableArrayViewDemo::tablearrayviewDemo2'
                   )
               )
        );

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Empty table array view'),
                $W->VBoxItems(
                    $this->tablearrayviewDemoEmpty()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem(
            $this->codeSection(
                array(
                    'widgetsDemo_CtrlTableArrayViewDemo::tablearrayviewDemoEmpty'
                )
            )
        );*/


        return $box;
    }



    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('TableArrayViews'));

        return $page;
    }




    /**
     * TableArrayView demo.
     *
     * @return Widget_TableArrayView
     */
    public function tablearrayview($id)
    {
        $W = bab_Widgets();

        // A simple tablearrayview.
        //---------------------------------------------------------------------


        $sampleSet = new widgetsDemo_SampleSet();

        $selections = $sampleSet->select($sampleSet->name->endsWith(substr($id, -1)));

        $head = array();
        $content = array();
        foreach($selections as $selection){
            $content[$selection->id] = array();
            $values = $selection->getValues();
            foreach($values as $k => $v){
                $content[$selection->id][$k] = $v;
                $head[$k] = $k;
            }
        }
        $tablearrayview = $W->TableArrayView($head, $content, null, null, $id);

        $tablearrayview->setAjaxAction();

        $tablearrayview->setReloadAction($this->proxy()->tablearrayview($id));

        return $tablearrayview;
    }


    /**
     * Export the current list of selected contact into an Excel spreadsheet.
     *
     * @param int $event
     */
    public function tablearrayviewExportXslx($id)
    {
        $tablearrayview = $this->tablearrayview($id);
        $tablearrayview->downloadXlsx();
    }


    /**
     * TableArrayView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tablearrayviewDemo1()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $tablearrayview1 = $this->tablearrayview('widgetsDemo_tablearrayviewDemo1');
        $box->addItem(
            $W->Link(
                widgetsDemo_translate('Xlsx Export'),
                $this->proxy()->tablearrayviewExportXslx('widgetsDemo_tablearrayviewDemo1')
            )->addClass('icon', Func_Icons::MIMETYPES_OFFICE_SPREADSHEET, 'widget-actionbutton')
            ->setSizePolicy(Func_Icons::ICON_LEFT_16)
        );
        $box->addItem(
            $W->HBoxItems(
                $tablearrayview1->setSizePolicy('widget-100pc')
            )->setHorizontalSpacing(2, 'em')
        );

        return $box;
    }


    /**
     * TableArrayView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tablearrayviewDemo2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $tablearrayview1 = $this->tablearrayview('widgetsDemo_tablearrayviewDemo2_1');
        $tablearrayview2 = $this->tablearrayview('widgetsDemo_tablearrayviewDemo2_2');
        $box->addItem(
            $W->HBoxItems(
                $tablearrayview1->setSizePolicy('widget-50pc'),
                $tablearrayview2->setSizePolicy('widget-50pc')
            )->setHorizontalSpacing(2, 'em')
        );

        return $box;
    }


    /**
     * TableArrayView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tablearrayviewDemoEmpty()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $tablearrayview = $W->TableArrayView('widgetsDemo_tablearrayviewDemo3_1');

        $box->addItem(
            $tablearrayview
        );

        return $box;
    }
}
