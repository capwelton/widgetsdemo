<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlInputDemo extends widgetsDemo_Controller
{



    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Buttons and links'),
                $W->VBoxItems(
                    $this->buttons()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::buttons'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Line inputs'),
                $W->VBoxItems(
                    $this->lineInputs()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::lineInputs'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Selects'),
                $W->VBoxItems(
                    $this->selects()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::selects'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Checkboxes'),
                $W->VBoxItems(
                    $this->checkboxes()
                    ),
                3
                )->setFoldable(true, true)
            );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::checkboxes'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Suggests'),
                $W->VBoxItems(
                    $this->suggests()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::suggests'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Pickers'),
                $W->VBoxItems(
                    $this->pickers()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::pickers'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('File pickers'),
                $W->VBoxItems(
                    $this->filePickers()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::filePickers'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Image croppers'),
                $W->VBoxItems(
                    $this->imageCroppers()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::imageCroppers'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Text edits'),
                $W->VBoxItems(
                    $this->textEdits()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlInputDemo::textEdits'));

        return $box;
    }


    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('Basic input widgets'));

        return $page;
    }




    /**
     * Buttons demo.
     *
     * @return Widget_VBoxLayout
     */
    public function buttons()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // An actionbutton link.
        //---------------------------------------------------------------------
        $link = $W->Link('A simple link', '?');
        $link->setOpenMode(Widget_Link::OPEN_DIALOG);
        $box->addItem(
            $this->labelledItem('A sample link', $link)
        );


        // An actionbutton link.
        //---------------------------------------------------------------------
        $link = $W->Link('Open calendar demo', $this->CalendarDemo()->demo());
        $link->addClass('widget-actionbutton');
        $link->setOpenMode(Widget_Link::OPEN_DIALOG);
        $box->addItem(
            $this->labelledItem('An actionbutton link', $link)
        );

        // An actionbutton link.
        //---------------------------------------------------------------------
        $link = $W->Link('Open calendar demo', $this->CalendarDemo()->demo());
        $link->addClass('widget-actionbutton ui-state-default ui-corner-all');
        $link->setOpenMode(Widget_Link::OPEN_DIALOG);
        $box->addItem(
            $this->labelledItem('An actionbutton link', $link)
        );

        // An actionbutton link.
        //---------------------------------------------------------------------
        $link = $W->Link('Open organizational chart demo', $this->OrgChartDemo()->demo());
        $link->addClass('widget-actionbutton', 'icon', Func_Icons::APPS_ORGCHARTS);
        $link->setSizePolicy(Func_Icons::ICON_LEFT_16);
        $link->setOpenMode(Widget_Link::OPEN_DIALOG);
        $box->addItem(
            $this->labelledItem('An actionbutton link', $link)
        );


        // A button.
        //---------------------------------------------------------------------
        $button = $W->Button();
        $button->addItem($W->Label('A button'));
        $box->addItem(
            $this->labelledItem('A button', $button)
        );

        return $box;
    }




    /**
     * Line inputs demo.
     *
     * @return Widget_VBoxLayout
     */
    public function lineInputs()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A simple single line editor.
        //---------------------------------------------------------------------
        $lineEdit = $W->LineEdit();
        $box->addItem(
            $this->labelledItem('Default line edit', $lineEdit)
        );


        // A single line email address editor.
        //---------------------------------------------------------------------
        $emailLineEdit = $W->EmailLineEdit();
        $box->addItem(
            $this->labelledItem('Email line edit', $emailLineEdit)
        );


        // A single line telephone number editor.
        //---------------------------------------------------------------------
        $telLineEdit = $W->TelLineEdit();
        $box->addItem(
            $this->labelledItem('Telephone line edit', $telLineEdit)
        );

        // A single international line telephone number editor.
        //---------------------------------------------------------------------
        $internationalTelLineEdit = $W->InternationalTelLineEdit()
            ->setCountries(array('fr','au','gb', 'us'))
            ->setPreferredCountries(array('gb'))
            ->setInitialCountry('us');
        $box->addItem(
            $this->labelledItem('International telephone line edit', $internationalTelLineEdit)
        );


        // A color picker.
        //---------------------------------------------------------------------
        $colorPicker = $W->ColorPicker();
        $box->addItem(
            $this->labelledItem('Color picker', $colorPicker)
        );


        // A Input password
        //---------------------------------------------------------------------
        $colorPicker = $W->InputPassword();
        $box->addItem(
            $colorPicker
        );
        return $box;
    }


    /**
     * Pickers demo.
     *
     * @return Widget_VBoxLayout
     */
    public function pickers()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A user picker.
        //---------------------------------------------------------------------
        $userPicker = $W->UserPicker();
        $userPicker->setName('user1');
        $userPicker->setSizePolicy(Func_Icons::ICON_LEFT_16);
        $box->addItem(
            $this->labelledItem('User picker', $userPicker)
        );

        // A group picker.
        //---------------------------------------------------------------------
        $groupPicker = $W->GroupPicker();
        $box->addItem(
            $this->labelledItem('Group picker', $groupPicker)
        );

        // An article topic picker.
        //---------------------------------------------------------------------
        $topicPicker = $W->TopicPicker();
        $box->addItem(
            $this->labelledItem('Article topic picker', $topicPicker)
        );

        // A sitemap item picker.
        //---------------------------------------------------------------------
        $sitemapItemPicker = $W->SitemapItemPicker();
        $box->addItem(
            $this->labelledItem('Sitemap item picker', $sitemapItemPicker)
        );

        // A bab file picker.
        //---------------------------------------------------------------------
        $babFilePicker = $W->BabFilePicker();
        $box->addItem(
            $this->labelledItem('bab File picker', $babFilePicker)
        );

        return $box;
    }


    /**
     * Pickers demo.
     *
     * @return Widget_VBoxLayout
     */
    public function filePickers()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A file picker.
        //---------------------------------------------------------------------
        $filePicker = $W->FilePicker();
        $filePicker->setName('file1');
        $filePicker->oneFileMode(true);
//         $filePicker->setMultiple(false);
        $filePicker->setSizePolicy(Func_Icons::ICON_LEFT_16);
        $box->addItem(
            $this->labelledItem('Single file picker', $filePicker)
        );

        // An image picker.
        //---------------------------------------------------------------------
        $imagePicker = $W->ImagePicker();
        $imagePicker->setName('image1');
        $imagePicker->oneFileMode(true);
        $box->addItem(
            $this->labelledItem('Single image picker', $imagePicker)
        );

        // An multiple image picker.
        //---------------------------------------------------------------------
        $imagePicker = $W->ImagePicker();
        $imagePicker->setName('image2');
        $imagePicker->oneFileMode(false);
        $imagePicker->setDimensions(64, 64);
        $imagePicker->setFileNameWidth(64, 'px');
//         $imagePicker->setMultiple(true);
        $box->addItem(
            $this->labelledItem('Multiple image picker with setDimensions and setFileNameWidth', $imagePicker)
        );


        return $box;
    }

    /**
     * Image cropper demo.
     *
     * @return Widget_VBoxLayout
     */
    public function imageCroppers()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // An image cropper
        //---------------------------------------------------------------------
        $imageCropper = $W->ImageCropper();
        $imageCropper->setName('imagecropper1');
        $T = bab_functionality::get('Thumbnailer');
        /* @var $T Func_Thumbnailer */

        $T->setSourceBinary(file_get_contents('http://www.momesdazur.com/images2/origami%20fleurs.jpg'), date('Y-m-d'));
        $imageCropper->setUrl($T->getThumbnail(9999, 9999));
        $imageCropper->setRatio(16/9);
        $imageCropper->setMinSize(800, 800);
        $box->addItem(
            $this->labelledItem('Image cropper with aspect ratio 16/9', $imageCropper)
        );

        // An image cropper
        //---------------------------------------------------------------------
        $imageCropper = $W->ImageCropper();
        $imageCropper->setName('imagecropper2');
        $T = bab_functionality::get('Thumbnailer');
        /* @var $T Func_Thumbnailer */

        $T->setSourceBinary(file_get_contents('http://www.momesdazur.com/images2/origami%20fleurs.jpg'), date('Y-m-d'));
        $imageCropper->setUrl($T->getThumbnail(9999, 9999));
        $imageCropper->setMinSize(640, 360);
        $box->addItem(
            $this->labelledItem('Image cropper', $imageCropper)
        );

        return $box;
    }


    /**
     * Radio menu demo.
     *
     * @return Widget_VBoxLayout
     */
    public function radioMenus()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        bab_functionality::includefile('Icons');
        $iconReflector = new ReflectionClass('Func_Icons');
        $constants = array_flip($iconReflector->getConstants());

        bab_Sort::natcasesort($constants);

        $radiomenu = $W->RadioMenu();
        $radiomenu->addClass(Func_Icons::ICON_LEFT_32);//->width(60, 'px');

        foreach ($constants as $key => $c) {
            $radiomenu->addOption($key, $W->Icon($c, eval('return Func_Icons::'.$c.';')));
        }

        $box->addItem(
            $this->labelledItem('Radio menu', $radiomenu)
        );
        return $box;
    }



    /**
     * Selects demo.
     *
     * @return Widget_VBoxLayout
     */
    public function selects()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A simple select.
        //---------------------------------------------------------------------
        $select = $W->Select();
        $select->addOption('option1', 'Option 1');
        $select->addOption('option2', 'Option 2');
        $select->addOption('option3', 'Option 3');
        $box->addItem(
            $this->labelledItem('Simple select', $select)
        );

        // A select with option groups.
        //---------------------------------------------------------------------
        $select = $W->Select();
        $select->addOption('option1.1', 'Option 1.1', 'Groupe 1');
        $select->addOption('option1.2', 'Option 1.2', 'Groupe 1');
        $select->addOption('option1.3', 'Option 1.3', 'Groupe 1');
        $select->addOption('option2.1', 'Option 2.1', 'Groupe 2');
        $select->addOption('option2.2', 'Option 2.2', 'Groupe 2');
        $select->addOption('option2.3', 'Option 2.3', 'Groupe 2');
        $box->addItem(
            $this->labelledItem('Select with option groups', $select)
        );

        // Linked selects.
        //---------------------------------------------------------------------
        $selectGroup = $W->Select();
        $selectGroup->addOption('1', 'Groupe 1');
        $selectGroup->addOption('2', 'Groupe 2');

        $select = $W->Select();
        $select->addOption('option1.1', 'Option 1.1', 'Groupe 1');
        $select->addOption('option1.2', 'Option 1.2', 'Groupe 1');
        $select->addOption('option1.3', 'Option 1.3', 'Groupe 1');
        $select->addOption('option2.1', 'Option 2.1', 'Groupe 2');
        $select->addOption('option2.2', 'Option 2.2', 'Groupe 2');
        $select->addOption('option2.3', 'Option 2.3', 'Groupe 2');
        $box->addItem(
            $W->FlowItems(
                $this->labelledItem('Select the group', $selectGroup),
                $this->labelledItem('Linked select', $select)
            )->setHorizontalSpacing(2, 'em')
        );

        $select->setAssociatedOptgroupSelect($selectGroup);


        // Multiselect.
        //---------------------------------------------------------------------
        $select = $W->MultiSelect();
        $select->addOption('option1', 'Option 1');
        $select->addOption('option2', 'Option 2');
        $select->addOption('option3', 'Option 3');
        $box->addItem(
            $this->labelledItem('Simple select', $select)
        );


        return $box;
    }




    /**
     * Checkboxes demo.
     *
     * @return Widget_VBoxLayout
     */
    public function checkboxes()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $cb1 = $W->CheckBox();
        $cb2 = $W->CheckBox();
        $cb3 = $W->CheckBox();
        $cb4 = $W->CheckBox();
        $cb5 = $W->CheckBox();
        $cb6 = $W->CheckBox();
        $cb7 = $W->CheckBox();
        $cb8 = $W->CheckBox();

        $cbAll = $W->CheckBoxAll();
        $cbAll->addCheckBox($cb1);
        $cbAll->addCheckBox($cb2);
        $cbAll->addCheckBox($cb3);
        $cbAll->addCheckBox($cb4);
        $box->addItem($this->labelledItem('Check all', $cbAll));


        $box->addItem(
            $this->labelledItem('Checkbox 1', $cb1)
        );
        $box->addItem(
            $this->labelledItem('Checkbox 2', $cb2)
        );
        $box->addItem(
            $this->labelledItem('Checkbox 3', $cb3)
        );
        $box->addItem(
            $this->labelledItem('Checkbox 4', $cb4)
        );


        return $box;
    }



    /**
     * Suggests demo.
     *
     * @return Widget_VBoxLayout
     */
    public function suggests()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A suggest place name.
        //---------------------------------------------------------------------
        $suggestPlaceName = $W->SuggestPlaceName();
        $suggestPlaceName->setMinChars(0);
        $box->addItem(
            $this->labelledItem('Suggest place name', $suggestPlaceName)
        );

        // A suggest place name with text transform uppercase.
        //---------------------------------------------------------------------
        $suggestPlaceName = $W->SuggestPlaceName();
        $suggestPlaceName->setMinChars(0);
        $suggestPlaceName->setConvertCase(MB_CASE_UPPER);
        $box->addItem(
            $this->labelledItem('Suggest place name uppercase', $suggestPlaceName)
        );

        // A suggest place name with text transform lowercase.
        //---------------------------------------------------------------------
        $suggestPlaceName = $W->SuggestPlaceName();
        $suggestPlaceName->setMinChars(2);
        $suggestPlaceName->setConvertCase(MB_CASE_LOWER);
        $box->addItem(
            $this->labelledItem('Suggest place name lowercase', $suggestPlaceName)
        );


        // A suggest place name with text transform uppercase, multiple values.
        //---------------------------------------------------------------------
        $suggestPlaceName = $W->SuggestPlaceName();
        $suggestPlaceName->setMinChars(2);
        $suggestPlaceName->setConvertCase(MB_CASE_UPPER);
        $suggestPlaceName->setMultiple(',');
        $box->addItem(
            $this->labelledItem('Suggest place name uppercase, multiple values', $suggestPlaceName)
        );


        // A suggest google place name
        //---------------------------------------------------------------------
        $suggestGoogle = $W->SuggestAddressGoogle()->setSize(60);
        $box->addItem(
            $this->labelledItem('Suggest Google place name ', $suggestGoogle)
        );


        // A suggestdirectory entry.
        //---------------------------------------------------------------------
        $suggestDirectoryEntry = $W->SuggestDirectoryEntry();
        $suggestDirectoryEntry->setMinChars(0);
        $box->addItem(
            $this->labelledItem('Suggest directory entry', $suggestDirectoryEntry)
        );


        // A SuggestUser.
        //---------------------------------------------------------------------
        $suggestUser = $W->SuggestUser();
        $suggestUser->setMinChars(0);
        $box->addItem(
            $this->labelledItem('Suggest user', $suggestUser)
        );


        return $box;
    }




    /**
     * Text edits demo.
     *
     * @return Widget_VBoxLayout
     */
    public function textEdits()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A simple text area.
        //---------------------------------------------------------------------
           $textEdit = $W->TextEdit();
        $box->addItem(
            $this->labelledItem('Default text edit', $textEdit)
        );

        // A simple text area with limits.
        //---------------------------------------------------------------------
        $textEdit = $W->TextEdit();
        $textEdit->setMaxWords(5);
        $textEdit->addClass('widget-autoresize');
        $box->addItem(
            $this->labelledItem('Text edit with limits', $textEdit)
        );

        // An html wysiwyg editor with simple formatting capabilities.
        //---------------------------------------------------------------------
           $simpleHtmlEdit = $W->SimpleHtmlEdit();
        $simpleHtmlEdit->addClass('widget-autoresize');
        $box->addItem(
            $this->labelledItem('Simple html edit', $simpleHtmlEdit)
        );


        // An html wysiwyg editor with simple formatting capabilities.
        //---------------------------------------------------------------------
        $simpleHtmlEdit = $W->SimpleHtmlEdit();
        $simpleHtmlEdit->setMaxWords(5);
        $simpleHtmlEdit->addClass('widget-autoresize');
        $box->addItem(
            $this->labelledItem('Simple html edit', $simpleHtmlEdit)
        );
        
        // An advanced wysiwyg editor.
        //---------------------------------------------------------------------
        $babHtmlEdit = $W->CKEditor();
        $box->addItem(
            $this->labelledItem('CKEditor', $babHtmlEdit)
        );


        return $box;
    }

}
