<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';

require_once dirname(__FILE__) . '/../set/sample.class.php';


/**
 *
 */
class widgetsDemo_CtrlTableModelViewDemo extends widgetsDemo_Controller
{




    public function demo()
    {
    	$W = bab_Widgets();

    	$box = $W->VBoxLayout();
    	$box->setVerticalSpacing(1, 'em');

    	$box->addItem(
    	    $section = $W->Section(
    	        widgetsDemo_translate('Sample table model view'),
    	        $W->VBoxItems(
    	            $this->tablemodelviewDemo1()
    	        ),
    	        3
    	    )->setFoldable(true)
    	);
    	$section->addContextMenu()->addItem(
    	    $this->codeSection(
    	        array(
    	            'widgetsDemo_CtrlTableModelViewDemo::tablemodelview',
    	            'widgetsDemo_CtrlTableModelViewDemo::tablemodelviewDemo1'
    	        )
    	    )
    	);

    	$box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Table model views'),
        		$W->VBoxItems(
        		    $this->tablemodelviewDemo2()
				),
        		3
    	    )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem(
       		$this->codeSection(
       			array(
       				'widgetsDemo_CtrlTableModelViewDemo::tablemodelview',
       				'widgetsDemo_CtrlTableModelViewDemo::tablemodelviewDemo2'
       			)
       		)
        );

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Empty table model view'),
                $W->VBoxItems(
                    $this->tablemodelviewDemoEmpty()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem(
            $this->codeSection(
                array(
                    'widgetsDemo_CtrlTableModelViewDemo::tablemodelviewDemoEmpty'
                )
            )
        );


        return $box;
    }



    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
        	return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('TableModelViews'));

        return $page;
    }




    /**
     * TableModelView demo.
     *
     * @return Widget_TableModelView
     */
    public function tablemodelview($id)
    {
    	$W = bab_Widgets();

    	// A simple tablemodelview.
    	//---------------------------------------------------------------------
    	$tablemodelview = $W->TableModelView($id);


    	$sampleSet = new widgetsDemo_SampleSet();

    	$selection = $sampleSet->select($sampleSet->name->endsWith(substr($id, -1)));

    	$tablemodelview->setDataSource($selection);

    	$tablemodelview->addDefaultColumns($sampleSet);
//    	$tablemodelview->setPageLength(10 + substr($id, -1) * 2);
    	$tablemodelview->allowColumnSelection();
    	$tablemodelview->setAjaxAction();

//    	$tablemodelview->displayNumberOfRows();

    	$tablemodelview->setReloadAction($this->proxy()->tablemodelview($id));

    	return $tablemodelview;
    }


    /**
     * Export the current list of selected contact into an Excel spreadsheet.
     *
     * @param int $event
     */
    public function tablemodelviewExportXslx($id)
    {
        $tablemodelview = $this->tablemodelview($id);
        $tablemodelview->downloadXlsx();
    }


    /**
     * TableModelView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tablemodelviewDemo1()
    {
    	$W = bab_Widgets();

    	$box = $W->VBoxItems();
    	$box->setVerticalSpacing(1, 'em');


    	$tablemodelview1 = $this->tablemodelview('widgetsDemo_tablemodelviewDemo1');
    	$box->addItem(
    	    $W->Link(
                widgetsDemo_translate('Xlsx Export'),
                $this->proxy()->tablemodelviewExportXslx('widgetsDemo_tablemodelviewDemo1')
    	    )->addClass('icon', Func_Icons::MIMETYPES_OFFICE_SPREADSHEET, 'widget-actionbutton')
    	    ->setSizePolicy(Func_Icons::ICON_LEFT_16)
    	);
    	$box->addItem(
    		$W->HBoxItems(
    			$tablemodelview1->setSizePolicy('widget-100pc')
    		)->setHorizontalSpacing(2, 'em')
    	);

    	return $box;
    }


    /**
     * TableModelView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tablemodelviewDemo2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $tablemodelview1 = $this->tablemodelview('widgetsDemo_tablemodelviewDemo2_1');
        $tablemodelview2 = $this->tablemodelview('widgetsDemo_tablemodelviewDemo2_2');
        $box->addItem(
            $W->HBoxItems(
                $tablemodelview1->setSizePolicy('widget-50pc'),
                $tablemodelview2->setSizePolicy('widget-50pc')
            )->setHorizontalSpacing(2, 'em')
        );

        return $box;
    }


    /**
     * TableModelView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tablemodelviewDemoEmpty()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $tablemodelview = $W->TableModelView('widgetsDemo_tablemodelviewDemo3_1');

        $box->addItem(
            $tablemodelview
        );

        return $box;
    }
}
