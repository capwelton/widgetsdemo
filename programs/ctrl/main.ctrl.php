<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlMain extends widgetsDemo_Controller
{


	public function demoFrame()
	{
		$W = bab_Widgets();

		$layout = $W->VBoxLayout('demo-frame');

		if (!isset($_SESSION['widgetsDemo']['demo'])) {
			$_SESSION['widgetsDemo']['demo'] = 'Input';
		}

		$demo = $_SESSION['widgetsDemo']['demo'] . 'Demo';
		if (!method_exists($this, $demo)) {
		    $_SESSION['widgetsDemo']['demo'] = 'Input';
		    $demo = 'InputDemo';
		}

		$layout->addItem($this->{$demo}(false)->demo());


		return $layout;
	}


	public function setDemo($demo)
	{
		$_SESSION['widgetsDemo']['demo'] = $demo;
		return true;
	}


	protected function demoEntry($name, $demoName, $action)
	{
	    $W = bab_Widgets();
	    $entry = $W->HBoxItems(
	        $W->Link(
	            $name,
	            $action
	        )->setAjaxAction($this->proxy()->setDemo($demoName), 'demo-frame')
	        ->setSizePolicy('widget-col-100pc'),
	        $W->Link(
	            $W->Html('&#9701;'),
	            $action
	        )->setOpenMode(Widget_Link::OPEN_DIALOG)
	        ->addClass('widget-align-right', 'widget-100pc')
	        //->setSizePolicy('widget-25pc')
	    )->setSizePolicy('widget-list-element');
	    return $entry;
	}

    public function display()
    {
        $W = bab_Widgets();

        $pageLayout = $W->VBoxLayout();
        $pageLayout->setVerticalSpacing(2, 'em');
        $page = $W->BabPage(null, $pageLayout);

        $page->setTitle(widgetsDemo_translate('Demo'));


        $demoList = $W->VBoxItems(
            $this->demoEntry('Inputs', 'Input', $this->InputDemo()->display()),
            $this->demoEntry('Date pickers', 'DatePicker', $this->DatePickerDemo()->display()),
            $this->demoEntry('Radio menus', 'RadioMenu', $this->RadioMenuDemo()->display()),
            $this->demoEntry('Maps', 'Map', $this->MapDemo()->display()),
            $this->demoEntry('Accordions', 'Accordion', $this->AccordionDemo()->display()),
            $this->demoEntry('Forms', 'Form', $this->FormDemo()->display()),
            $this->demoEntry('Calendars', 'Calendar', $this->CalendarDemo()->display()),
            $this->demoEntry('Timelines', 'Timeline', $this->TimelineDemo()->display()),
            $this->demoEntry('Treeviews', 'Treeview', $this->TreeviewDemo()->display()),
            $this->demoEntry('Organizational charts', 'OrgChart', $this->OrgChartDemo()->display()),
            $this->demoEntry('Table Views', 'TableView', $this->TableViewDemo()->display()),
            $this->demoEntry('Table Model Views', 'TableModelView', $this->TableModelViewDemo()->display()),
            $this->demoEntry('Popup Menus', 'PopupMenu', $this->PopupMenuDemo()->display()),
            $this->demoEntry('Portlets', 'Portlet', $this->PortletDemo()->display()),
            $this->demoEntry('Authentication', 'Authentication', $this->AuthenticationDemo()->display()),
            $this->demoEntry('Notification', 'Notification', $this->NotificationDemo()->display())
        );


        $demoFrame = $this->demoFrame();
        $demoFrame->setReloadAction($this->proxy()->demoFrame());


        $page->addItem(
        	$W->FlowItems(
        		$demoList->setSizePolicy('widget-25pc'),
        		$demoFrame->setSizePolicy('widget-75pc')
        	)->setVerticalAlign('top')
        	->setSpacing(2, 'em')
        );




        return $page;
    }





}
