<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 * 
 */
class widgetsDemo_CtrlAccordionDemo extends widgetsDemo_Controller
{

    

    public function demo()
    {
        $W = bab_Widgets();
        
        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Simple accordions'),
        		$W->VBoxItems(
        			//$W->DelayedItem($this->proxy()->persistentForm())
        		    $this->simpleAccordions()
				),
        		3
    	    )->setFoldable(true)
        );
        
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlAccordionDemo::simpleAccordions'));
        
        
        
        $sourceEditor = $this->simpleAccordions();
        
        $addSourceButton = $W->VboxItems(
            $W->Link('Click', '')
            ->addClass('widget-instant-button widget-actionbutton'),
            $sourceEditor
            ->setTitle('Click')
            ->addClass('widget-instant-form')
        )->addClass('widget-instant-container');
        
        
        
        $box->addItem($addSourceButton);
        
        return $box;
    }


    public function display()
    {
    	$box = $this->demo();
    	if (bab_isAjaxRequest()) {
    		return $box;
    	}
    	 
    	$W = bab_Widgets();
    	 
    	$page = $W->BabPage(null, $box);
    	 
    	$page->setTitle(widgetsDemo_translate('Accordions demo'));
    	 
    	return $page;
    }

    
    
    protected function Demo_accordionsListView($id = null)
    {
        $W = Demo_widgetFactory();
    
        $listView = $W->ListView($id)->addClass('widget-configuration-panel');
    
        if (($I = bab_functionality::get('Icons')) === false) {
            return $listView;
        }
    
        $I->includeCss();
    
        $listView->addItem($W->Icon('Configuration du site', Func_Icons::APPS_PREFERENCES_SITE))
            ->addItem($W->Icon('Mail server parameters', Func_Icons::APPS_PREFERENCES_MAIL_SERVER))
            ->addItem($W->Icon('User options', Func_Icons::APPS_PREFERENCES_USER))
            ->addItem($W->Icon('Serveur uploads configuration', Func_Icons::PLACES_FOLDER))
            ->addItem($W->Icon('Date and time format', Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT))
            ->addItem($W->Icon('Calendar and vacation options', Func_Icons::APPS_CALENDAR))
            ->addItem($W->Icon('Authentication configuration', Func_Icons::APPS_PREFERENCES_AUTHENTICATION))
            ->addItem($W->Icon('Registration configuration', Func_Icons::APPS_DIRECTORIES))
            ->addItem($W->Icon('Wysiwyg editor parameters', Func_Icons::APPS_PREFERENCES_WYSIWYG_EDITOR))
            ->addItem($W->Icon('Search engine configuration', Func_Icons::APPS_PREFERENCES_SEARCH_ENGINE))
            ->addItem($W->Icon('Web services', Func_Icons::APPS_PREFERENCES_WEBSERVICES))
        ;
    
        return $listView;
    }

    
    /**
     * Accordions demo.
     *
     * @return Widget_VBoxLayout
     */
    public function simpleAccordions()
    {
    	$W = bab_Widgets();

    	 
    	$accordions = $W->Accordions('demo-simple-accordions');
    	
    	$accordions->setLayout($W->VBoxLayout());
    	
    	
    	$accordions->addPanel('Configuration panel: icons on left / 48px', $this->Demo_accordionsListView()->addClass(Func_Icons::ICON_LEFT_48));
    	
    	$accordions->addPanel('Configuration panel: icons on top / 32px', $this->Demo_accordionsListView()->addClass(Func_Icons::ICON_TOP_32));
    	
    	$accordions->addPanel('Configuration panel: icons on left / 24px', $this->Demo_accordionsListView()->addClass(Func_Icons::ICON_LEFT_24));
    	
    	$accordions->addPanel('Configuration panel: icons on top / 16px', $this->Demo_accordionsListView()->addClass(Func_Icons::ICON_TOP_16));
    	 
    	return $accordions;
    }
    

   
   
}
