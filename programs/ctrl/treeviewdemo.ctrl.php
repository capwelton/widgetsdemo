<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 * 
 */
class widgetsDemo_CtrlTreeviewDemo extends widgetsDemo_Controller
{
    
    

    
    
    
    
    
    
    public function demo()
    {
    	$W = bab_Widgets();
    	
    	$box = $W->VBoxLayout();
    	$box->setVerticalSpacing(1, 'em');
    	
    	$box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Sample treeview'),
        		$W->VBoxItems(
        		    $this->treeview()
				),
        		3
    	    )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlTreeviewDemo::treeview'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Sample treeview 2'),
                $W->VBoxItems(
                    $this->treeview2()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlTreeviewDemo::treeview2'));

        return $box;
    }
    
    

    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
        	return $box;
        }

        $W = bab_Widgets();
        
        $page = $W->BabPage(null, $box);
        
        $page->setTitle(widgetsDemo_translate('Treeviews'));
        
        return $page;
    }





    /**
     * Treeview demo.
     *
     * @return Widget_VBoxLayout
     */
    public function treeview()
    {
    	$W = bab_Widgets();
    	 
    	$box = $W->VBoxItems();
    	$box->setVerticalSpacing(1, 'em');


    	// A simple treeview.
    	//---------------------------------------------------------------------
    	$treeview = $W->SimpleTreeview('test_treeview');
    	
    	$treeview->addClass(Func_Icons::ICON_LEFT_16);
    	
    	$treeview->hideToolbar()
    		->setPersistent(true);
    
    	$root = $treeview->createElement('root');

       	$icon = $W->Icon(widgetsDemo_translate('Root node'), Func_Icons::OBJECTS_GROUP);
    	
    	$root->setItem($icon);
    	
    	$treeview->appendElement($root, null);

    	
    	
    	for ($i = 0; $i < 10; $i++) {
    	    $subNode = $treeview->createElement('subNode' . $i);
    	     
    	    $icon = $W->Icon(widgetsDemo_translate('Sub node ' . $i), Func_Icons::PLACES_FOLDER);
    	    
    	    $subNode->setItem($icon);
    	    
    	    $treeview->appendElement($subNode, 'root');
    	    
    	    for ($j = 0; $j < $i; $j++) {
    	        $subNode = $treeview->createElement('subSubNode' . $i . '.' . $j);
    	    
    	        $icon = $W->Icon(widgetsDemo_translate('Sub sub node ' . $i . '.' . $j), Func_Icons::PLACES_FOLDER);
    	        $subNode->setItem($icon);
    	        
    	        $treeview->appendElement($subNode, 'subNode' . $i);
    	    }
    	}
    	
    	$box->addItem(
    		$treeview
    	);

    	return $box;
    }







    /**
     * Treeview demo.
     *
     * @return Widget_VBoxLayout
     */
    public function treeview2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A simple treeview.
        //---------------------------------------------------------------------
        $treeview = $W->SimpleTreeview('test_treeview2');

        $treeview->addClass(Func_Icons::ICON_LEFT_16);

        $treeview->hideToolbar()
            ->setPersistent(true);

        $root = $treeview->createElement('root');

        $icon = $W->Icon(widgetsDemo_translate('Root node'), Func_Icons::OBJECTS_GROUP);

        $root->setItem($icon);
         
        $treeview->appendElement($root, null);


        for ($i = 0; $i < 10; $i++) {
            $subNode = $treeview->createElement('subNode' . $i);

            $subNode->setItem(
                $W->FlowItems(
                    $W->Label('Test')->addClass('icon', Func_Icons::ACTIONS_MAIL_SEND),
                    $W->Label('Test2')->addClass('widget-actionbutton', 'icon', Func_Icons::MIMETYPES_PACKAGE_X_GENERIC)
                )
            );

            $treeview->appendElement($subNode, 'root');

        }
    
         
        $box->addItem(
            $treeview
        );
    
        return $box;
    }
}
