<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';

require_once dirname(__FILE__) . '/../set/sample.class.php';


/**
 *
 */
class widgetsDemo_CtrlPopupMenuDemo extends widgetsDemo_Controller
{




    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Popup menus'),
                $W->VBoxItems(
                    $this->popupMenuDemo1()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem(
            $this->codeSection(
                array(
                    'widgetsDemo_CtrlPopupMenuDemo::popupMenuDemo1'
                )
            )
        );


        return $box;
    }




    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('PopupMenus'));

        return $page;
    }







    /**
     * PopupMenu demo.
     *
     * @return Widget_VBoxLayout
     */
    public function popupMenuDemo1()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $menu = $W->Menu('widgetsDemo_popupMenuDemo1');
        $menu->setLayout($W->VBoxLayout());
        $menu->setButtonClass('btn btn-default btn-xs');
        $menu->addItem($W->Label('Text 1'));
        $menu->addItem($W->Label('Text 2'));
        $menu->addItem($W->Label('Text 3'));
        $menu->addSeparator();
        $menu->addItem($W->Label('Text 4'));

        $box->addItem(
           $menu
        );

        return $box;
    }
}
