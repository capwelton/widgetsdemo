<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';

require_once dirname(__FILE__) . '/../set/sample.class.php';



/**
 *
 */
class widgetsDemo_CtrlTableViewDemo extends widgetsDemo_Controller
{




    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Sample table model view'),
                $W->VBoxItems(
                    $this->tableviewDemo1()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem(
            $this->codeSection(
                array(
                    'widgetsDemo_CtrlTableViewDemo::tableview',
                    'widgetsDemo_CtrlTableViewDemo::tableviewDemo1'
                )
            )
        );

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Table model views'),
                $W->VBoxItems(
                    $this->tableviewDemo2()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem(
            $this->codeSection(
                array(
                    'widgetsDemo_CtrlTableViewDemo::tableview',
                    'widgetsDemo_CtrlTableViewDemo::tableviewDemo2'
                )
            )
        );

        return $box;
    }




    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('TableViews'));

        return $page;
    }




    /**
     * TableView demo.
     *
     * @return Widget_TableView
     */
    public function tableview($id, $nbRows = 12)
    {
        $W = bab_Widgets();

        // A simple tableview.
        //---------------------------------------------------------------------
        $tableview = $W->TableView($id);


        $row = 0;

        $tableview->addSection('header', null, 'widget-table-header');
        $tableview->setCurrentSection('header');

        $tableview->addItem(
            $W->Label(widgetsDemo_translate('Month')),
            $row,
            0
        );
        $tableview->addItem(
            $W->Label(widgetsDemo_translate('Total amount')),
            $row,
            1
        );
//         $tableview->addItem(
//             $W->Label(widgetsDemo_translate('Total amount')),
//             $row,
//             2
//             );
//         $tableview->addItem(
//             $W->Label(widgetsDemo_translate('Total amount')),
//             $row,
//             3
//             );

        srand(date('Y-m-d'));

        $W->includePhpClass('Widget_Color');

        $tableview->setColumnChartColor(1, Widget_Color::DEEPPURPLE);
        $tableview->setColumnChartColor(2, Widget_Color::GREEN);
        $tableview->setColumnChartColor(3, Widget_Color::ORANGE);

         $tableview->setRowChartColor(1, Widget_Color::PINK);
         $tableview->setRowChartColor(2, Widget_Color::LIME);
         $tableview->setRowChartColor(3, Widget_Color::CYAN);
         $tableview->setRowChartColor(4, Widget_Color::INDIGO);
//         $tableview->setRowChartColor(5, Widget_Color::TEAL);
//         $tableview->setRowChartColor(6, Widget_Color::DEEPPURPLE);
//         $tableview->setRowChartColor(7, Widget_Color::ORANGE);
//         $tableview->setRowChartColor(8, Widget_Color::GREEN);

        $tableview->addSection('body', null, 'widget-table-body');
        $tableview->setCurrentSection('body');

        for ($i = 0; $i < $nbRows; $i++) {

            $label = bab_DateStrings::getMonth(($i % 12) + 1);
            $amount = rand(10000, 50000);

            $tableview->addItem(
                $W->Label($label),
                $row,
                0
            );
            $tableview->addItem(
                $W->Link($amount, $this->proxy()->tableview($id, $nbRows))
                    ->setOpenMode(Widget_Link::OPEN_DIALOG),
                $row,
                1
            );
//             $amount = rand(10000, 50000);
//             $tableview->addItem(
//                 $W->Link($amount, $this->proxy()->tableview($id, $nbRows))
//                     ->setOpenMode(Widget_Link::OPEN_DIALOG),
//                 $row,
//                 2
//             );
//             $amount = rand(10000, 50000);
//             $tableview->addItem(
//                 $W->Link($amount, $this->proxy()->tableview($id, $nbRows))
//                     ->setOpenMode(Widget_Link::OPEN_DIALOG),
//                 $row,
//                 3
//             );
            $row++;
        }


        return $tableview;
    }


    /**
     * TableView demo.
     *
     * @return Widget_TableView
     */
    public function tableview2($id, $nbRows = 12, $nbCols = 3)
    {
        $W = bab_Widgets();

        // A simple tableview.
        //---------------------------------------------------------------------
        $tableview = $W->TableView($id);


        $row = 0;
        $col = 0;

        $tableview->addSection('header', null, 'widget-table-header');
        $tableview->setCurrentSection('header');

        $tableview->addItem(
            $W->Label(widgetsDemo_translate('Month')),
            $row,
            $col
        );
        $col++;
        for ($y = 2010; $y < 2010 + $nbCols; $y++) {
            $tableview->addItem(
                $W->Label(widgetsDemo_translate($y)),
                $row,
                $col
            );
            $col++;
        }

        $W->includePhpClass('Widget_Color');

        $tableview->setColumnChartColor(1, Widget_Color::PINK);
        $tableview->setColumnChartColor(2, Widget_Color::LIME);
        $tableview->setColumnChartColor(3, Widget_Color::CYAN);
        $tableview->setColumnChartColor(4, Widget_Color::INDIGO);
        $tableview->setColumnChartColor(5, Widget_Color::TEAL);
        $tableview->setColumnChartColor(6, Widget_Color::DEEPPURPLE);
        $tableview->setColumnChartColor(7, Widget_Color::ORANGE);
        $tableview->setColumnChartColor(8, Widget_Color::GREEN);

        $tableview->addSection('body', null, 'widget-table-body');
        $tableview->setCurrentSection('body');


        srand(date('Y-m-d'));

        for ($i = 0; $i < $nbRows; $i++) {

            $label = bab_DateStrings::getMonth(($i % 12) + 1);

            $col = 0;

            $tableview->addItem(
                $W->Label($label),
                $row,
                $col
            );
            $col++;

            for ($y = 2010; $y < 2010 + $nbCols; $y++) {
                $amount = rand(35000, 50000);
                $tableview->addItem(
                    $W->Link($amount, $this->proxy()->tableview($id, $nbRows, $nbCols))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $row,
                    $col
                );
                $col++;
            }

            $row++;
        }

        return $tableview;
    }


    /**
     * Export the current list of selected contact into an Excel spreadsheet.
     *
     * @param int $event
     */
    public function tableviewExportXslx($id)
    {
        $tableview = $this->tableview($id);
        $tableview->downloadXlsx();
    }


    /**
     * TableView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tableviewDemo1()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $tableview1 = $this->tableview('widgetsDemo_tableviewDemo1');
        $box->addItem(
            $W->HBoxItems(
                $tableview1->setSizePolicy('widget-100pc')
            )->setHorizontalSpacing(2, 'em')
        );

        return $box;
    }


    /**
     * TableView demo.
     *
     * @return Widget_VBoxLayout
     */
    public function tableviewDemo2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $tableview1 = $this->tableview('widgetsDemo_tableviewDemo2_1', 4);
        $tableview1->setView(Widget_TableView::VIEW_CHART_DOUGHNUT);

        $tableview2 = $this->tableview('widgetsDemo_tableviewDemo2_2', 7);
        $tableview2->setView(Widget_TableView::VIEW_CHART_PIE);

        $tableview7 = $this->tableview('widgetsDemo_tableviewDemo2_7');
        $tableview7->setView(Widget_TableView::VIEW_CHART_POLARAREA);


        $tableview3 = $this->tableview2('widgetsDemo_tableviewDemo2_3');
        $tableview3->setView(Widget_TableView::VIEW_CHART_BAR);
        $tableview3->addClass(Widget_TableView::CHART_DATA_SERIES_IN_COLUMNS);

        $tableview35 = $this->tableview2('widgetsDemo_tableviewDemo2_35');
        $tableview35->setView(Widget_TableView::VIEW_CHART_BAR);
        $tableview35->addClass(Widget_TableView::CHART_DATA_SERIES_IN_ROWS);

        $tableview4 = $this->tableview2('widgetsDemo_tableviewDemo2_4', 6, 8);
        $tableview4->setView(Widget_TableView::VIEW_CHART_STACKEDBAR);
        $tableview4->addClass(Widget_TableView::CHART_DATA_SERIES_IN_COLUMNS);


        $tableview5 = $this->tableview2('widgetsDemo_tableviewDemo2_5');
        $tableview5->setView(Widget_TableView::VIEW_CHART_LINE);
        $tableview5->addClass(Widget_TableView::CHART_DATA_SERIES_IN_COLUMNS);

        $tableview5->addColumnChartStyle(1, 'nofill');
        $tableview5->addColumnChartStyle(1, 'dashed');

        $tableview5->addColumnChartStyle(2, 'nofill');
        $tableview5->addColumnChartStyle(2, 'dotted');

        $tableview5->addColumnChartStyle(3, 'fill');
        $tableview5->addColumnChartStyle(3, 'dashed');

        $tableview6 = $this->tableview2('widgetsDemo_tableviewDemo2_6');
        $tableview6->setView(Widget_TableView::VIEW_CHART_RADAR);
        $tableview6->addClass(Widget_TableView::CHART_DATA_SERIES_IN_COLUMNS);

        $tableview6->addColumnChartStyle(1, 'nofill');
        $tableview6->addColumnChartStyle(1, 'dashed');

        $tableview6->addColumnChartStyle(2, 'nofill');
        $tableview6->addColumnChartStyle(2, 'dotted');

        $tableview6->addColumnChartStyle(3, 'fill');
        $tableview6->addColumnChartStyle(3, 'dashed');

        $box->addItem(
            $W->FlowItems(
                $W->LabelledWidget('Doughnut chart', $tableview1)->setSizePolicy('widget-50pc'),
                $W->LabelledWidget('Pie chart', $tableview2)->setSizePolicy('widget-50pc'),
            	$W->LabelledWidget('Polar area chart', $tableview7)->setSizePolicy('widget-100pc'),
                $W->LabelledWidget('Bar chart', $tableview3)->setSizePolicy('widget-100pc'),
                $W->LabelledWidget('Bar chart', $tableview35)->setSizePolicy('widget-100pc'),
                $W->LabelledWidget('Stacked bar chart', $tableview4)->setSizePolicy('widget-50pc'),
                $W->LabelledWidget('Line chart', $tableview5)->setSizePolicy('widget-50pc'),
                $W->LabelledWidget('Radar chart', $tableview6)->setSizePolicy('widget-100pc')
            )->setHorizontalSpacing(2, 'em')
        );

        return $box;
    }
}
