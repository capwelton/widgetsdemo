<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlRadioMenuDemo extends widgetsDemo_Controller
{
    
    
    
    public function demo()
    {
        $W = bab_Widgets();
        
        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');
        


        $box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Radio menus'),
        		$W->VBoxItems(
        		    $this->radioMenus()
        		),
        		3
        	)->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlRadioMenuDemo::radioMenus'));
        
        $box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Radio menus'),
        		$W->VBoxItems(
        		    $this->radioMenus2()
        		),
        		3
        	)->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlRadioMenuDemo::radioMenus2'));
        
        return $box;
    }
    


    public function display()
    {
    	$box = $this->demo();
    	if (bab_isAjaxRequest()) {
    		return $box;
    	}
    	
    	$W = bab_Widgets();
    	
    	$page = $W->BabPage(null, $box);
    	
    	$page->setTitle(widgetsDemo_translate('Radio menu widgets'));
    	
    	return $page;
    }




    
    
    /**
     * Radio menu demo.
     *
     * @return Widget_VBoxLayout
     */
    public function radioMenus()
    {
        $W = bab_Widgets();
        
        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');
        
        bab_functionality::includefile('Icons');
        $iconReflector = new ReflectionClass('Func_Icons');
        $constants = array_flip($iconReflector->getConstants());
        
        bab_Sort::natcasesort($constants);
        
        $radiomenu = $W->RadioMenu();
        $radiomenu->addClass(Func_Icons::ICON_LEFT_32);
        
        foreach ($constants as $key => $c) {
            $radiomenu->addOption($key, $W->Icon($c . "\n" . str_replace('_', '-', strtolower($c)), eval('return Func_Icons::'.$c.';')));
        }
    
        $box->addItem(
            $this->labelledItem('Radio menu', $radiomenu)
        );
        return $box;
    }








    /**
     * Radio menu demo.
     *
     * @return Widget_VBoxLayout
     */
    public function radioMenus2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $radiomenu = $W->RadioMenu();
        $radiomenu->addClass('widget-20em');
        $radiomenu->addOption(
            'item1',
            $W->VBoxItems(
                $W->Label('First item')->addClass('widget-strong'),
                $W->Label('This is a description')->addClass('widget-small')
            )
        );
        $radiomenu->addOption(
            'item2',
            $W->VBoxItems(
                $W->Label('Second item')->addClass('widget-strong'),
                $W->Label('This is a description')->addClass('widget-small')
            )
        );
        $radiomenu->addOption(
            'item3',
            $W->VBoxItems(
                $W->Label('Third item')->addClass('widget-strong'),
                $W->Label('This is a description')->addClass('widget-small')
            )
        );

        $box->addItem(
            $this->labelledItem('Radio menu', $radiomenu)
        );
        return $box;
    }
}
