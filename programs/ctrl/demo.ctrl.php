<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 * 
 */
class widgetsDemo_CtrlDemo extends widgetsDemo_Controller
{
    
    
    public function setCurrentIconClass($iconClass)
    {
        $W = bab_Widgets();
        $W->setUserConfiguration('currentIconClass', $iconClass, 'widgetsDemo');
        die;
    }
    
    
    protected function getCurrentIconClass()
    {
        $W = bab_Widgets();
        $iconClass = $W->getUserConfiguration('currentIconClass', 'widgetsDemo');
        return isset($iconClass) ? $iconClass : Func_Icons::ICON_LEFT_48;
    }
    
    
    
    
    
    /**
     * 
     * @param string $id
     * 
     * @return Widget_Frame
     */
    public function listview($id = null)
    {
        $W = bab_Widgets();

        $listview = $W->Frame($id);
        $listview->addClass('widget-configuration-panel');
        
        $a = $this->getCurrentIconClass();
        if ($a == Func_Icons::ICON_LEFT_48) {
	        $listview->addItem($W->Icon('Configuration du site', Func_Icons::APPS_PREFERENCES_SITE))
	            ->addItem($W->Icon('Mail server parameters', Func_Icons::APPS_PREFERENCES_MAIL_SERVER))
	            ->addItem($W->Icon('User options', Func_Icons::APPS_PREFERENCES_USER))
	            ->addItem($W->Icon('Serveur uploads configuration', Func_Icons::PLACES_FOLDER))
	            ->addItem($W->Icon('Date and time format', Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT));
		}
	            
		$listview
            ->addItem($W->Icon('Calendar and vacation options', Func_Icons::APPS_CALENDAR))
            ->addItem($W->Icon('Authentication configuration', Func_Icons::APPS_PREFERENCES_AUTHENTICATION))
            ->addItem($W->Icon('Registration configuration', Func_Icons::APPS_DIRECTORIES))
            ->addItem($W->Icon('Wysiwyg editor parameters', Func_Icons::APPS_PREFERENCES_WYSIWYG_EDITOR))
            ->addItem($W->Icon('Search engine configuration', Func_Icons::APPS_PREFERENCES_SEARCH_ENGINE))
            ->addItem($W->Icon('Web services', Func_Icons::APPS_PREFERENCES_WEBSERVICES));

        $listview->addClass($this->getCurrentIconClass());
        
     
        // We set the reload action to the current controller method.
        $listview->setReloadAction(
            $this->proxy()->getMethodAction(__FUNCTION__, func_get_args())
        );
        
        return $listview;
    }
  

    
    
    
    

    public function demo1()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
    
        $configFrame = $W->Frame();
        
        $listview1 = $this->listview('widgetsdemo-listview1');
        $listview2 = $this->listview('widgetsdemo-listview2');
        
        $toolbar = $W->FlowItems(
            $W->Link('Change size 48')->addClass('bab_toolbarItem widget-actionbutton')
                ->setAjaxAction($this->proxy()->setCurrentIconClass(Func_Icons::ICON_LEFT_48), $listview1),
            $W->Link('Change size 16')->addClass('bab_toolbarItem widget-actionbutton')
            ->setAjaxAction($this->proxy()->setCurrentIconClass(Func_Icons::ICON_LEFT_16), $listview1),
            $W->Link('Change size 32')->addClass('bab_toolbarItem widget-actionbutton')
                ->setAjaxAction($this->proxy()->setCurrentIconClass(Func_Icons::ICON_TOP_32), $listview1)
        )->addClass('bab_toolbar');
        
    
        $configFrame->addItem(
            $W->Section(
                'Section containing tabs',
                $W->Tabs()->addTab(
                    'Listview',
                    $listview1
                )->addTab(
                    'Listview',
                   $listview2
                )
            )->setFoldable(true)
        );
    
        $page->addItem($toolbar);
        $page->addItem($configFrame);
    

        
        return $page;
    }
    

}
