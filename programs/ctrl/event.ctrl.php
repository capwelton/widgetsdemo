<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/calapi.php';

/**
 *
 */
class widgetsDemo_CtrlEvent extends widgetsDemo_Controller
{

    /**
     * Returns periods between $start and $end.
     *
     * @param bab_DateTime $start
     * @param bab_DateTime $end
     * @throws Exception
     *
     * @return bab_UserPeriods
     */
    public static function getCalendarEvents(bab_DateTime $start, bab_DateTime $end, $calids = null)
    {
        include_once $GLOBALS['babInstallPath'].'utilit/cal.userperiods.class.php';

        if (!isset($calids)) {
            $calids = array('personal/' . bab_getUserId());
        }

        $userPeriods = new bab_UserPeriods($start, $end);

        $factory = bab_getInstance('bab_PeriodCriteriaFactory');
        /* @var $factory bab_PeriodCriteriaFactory */

        $criteria = $factory->Collection(
            array(
                'bab_NonWorkingDaysCollection',
                'bab_VacationPeriodCollection',
                'bab_CalendarEventCollection',
                'bab_InboxEventCollection'
            )
        );

        $calendars = array();
        foreach ($calids as $idcal) {

            $calendar = bab_getICalendars()->getEventCalendar($idcal);
            if (!$calendar) {
                throw new Exception('Calendar not found for identifier : (' . $idcal . ')');
            }
            $calendars[] = $calendar;
        }

        $criteria = $criteria->_AND_($factory->Calendar($calendars));

        $userPeriods->createPeriods($criteria);
        $userPeriods->orderBoundaries();

        return $userPeriods;
    }




    /**
     *
     * @param bab_CalendarPeriod $event
     * @return mobical_Event
     */
    public static function fromCalendarPeriod(bab_CalendarPeriod $event, $readonly = false)
    {
        include_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';

        static $eventCategories = null;
        if (!isset($eventCategories)) {
            $eventCategories = array();
            $categories = bab_calGetCategories();
            foreach ($categories as $category) {
                $eventCategories[$category['name']] = $category;
            }
        }

        $eventCollection = $event->getCollection();
        $calendar = $eventCollection->getCalendar();


        if ($calendar != null) {

            $calendarId = $calendar->getUrlIdentifier();
            $eventUid = $event->getProperty('UID');
            $startDatetime = $event->getProperty('DTSTART');
            $endDatetime = $event->getProperty('DTEND');

            $evt = array(
                'id' => $calendarId . ':' . $eventUid,
                'allDay' => false,
                'start' => substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2),
                'end' => substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2),
                'category' => $event->getProperty('CATEGORIES'),
                'editable' => (!$readonly) && $calendar->canUpdateEvent($event)
            );
            if ($calendar->canViewEventDetails($event)) {
                $evt['title'] = $event->getProperty('SUMMARY');
                $evt['description'] = $event->getProperty('DESCRIPTION');
                $evt['location'] = $event->getProperty('LOCATION');
            }


            $color = $event->getProperty('X-CTO-COLOR');
            if (!empty($color)) {
                $evt['color'] = '#' . $color;
            } else if (isset($eventCategories[$evt->category])) {
                $evt['color'] = '#' . $eventCategories[$evt->category]['color'];
            }

        } else {

            $evt = array();

            $evt['id'] = null;
            $evt['creatorId'] = null;
            $evt['allDay'] = false;
            $evt['title'] = $event->getProperty('SUMMARY');
            $evt['description'] = $event->getProperty('DESCRIPTION');
            $evt['location'] = $event->getProperty('LOCATION');
            $startDatetime = $event->getProperty('DTSTART');
            $evt['startDatetime'] = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
            $endDatetime = $event->getProperty('DTEND');
            $evt['endDatetime'] = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);
            $evt['calendarId'] = null;
            $evt['backgroundColor'] = "#FFFFFF";
            $evt['category'] = $event->getProperty('CATEGORIES');

            $evt['editable'] = (!$readonly) && !($eventCollection instanceof bab_ReadOnlyCollection);
        }

        return $evt;
    }




    /**
     * Returns a json encoded array of events
     *
     * @param string	$inputDateFormat	The format of start and end parameters.
     * @param string	$start				Start date/time
     * @param string	$end				End date/time
     *
     * @return Widget_Action
     */
    public function events($calendar = null, $inputDateFormat = 'timestamp', $start = null, $end = null)
    {

        if ($inputDateFormat === 'timestamp') {
            $startDate = BAB_DateTime::fromTimeStamp($start);
            $endDate = BAB_DateTime::fromTimeStamp($end);
        } else {
            $startDate = BAB_DateTime::fromIsoDateTime($start);
            $endDate = BAB_DateTime::fromIsoDateTime($end);
        }

        if (isset($calendar)) {
            $calendarIds = array($calendar => $calendar);
        } else {
            $calendarIds = null;
        }

        $calendarEvents = self::getCalendarEvents($startDate, $endDate, $calendarIds);

        $jsonEvents = array();
        foreach ($calendarEvents as $calendarEvent) {
            $jsonEvents[] = self::fromCalendarPeriod($calendarEvent);
        }

        header('Content-type: application/json');
        echo bab_json_encode($jsonEvents);

        die;
    }





}
