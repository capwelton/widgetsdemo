<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


require_once dirname(__FILE__) . '/../set/formdemo.class.php';


/**
 *
 */
class widgetsDemo_CtrlFormDemo extends widgetsDemo_Controller
{



    public function demo($formDemoId = null)
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Slow ajax form'),
                $W->VBoxItems(
                    //$W->DelayedItem($this->proxy()->persistentForm())
                    $this->slowAjaxForm()
                ),
            3
            )->setFoldable(true)
            );
        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Form'),
                $W->VBoxItems(
                    //$W->DelayedItem($this->proxy()->persistentForm())
                    $this->form($formDemoId)
                ),
                3
            )->setFoldable(true)
        );
        $box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Persistent form'),
        		$W->VBoxItems(
        			//$W->DelayedItem($this->proxy()->persistentForm())
        		    $this->persistentForm()
				),
        		3
    	    )->setFoldable(true)
        );

        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlFormDemo::persistentForm'));

        return $box;
    }


    public function display($formDemoId = null)
    {
    	$box = $this->demo($formDemoId);
    	if (bab_isAjaxRequest()) {
    		return $box;
    	}

    	$W = bab_Widgets();

    	$page = $W->BabPage(null, $box);

    	$page->setTitle(widgetsDemo_translate('Forms demo'));

    	return $page;
    }


    /**
     * Line inputs demo.
     *
     * @return Widget_VBoxLayout
     */
    public function persistentForm()
    {
    	$W = bab_Widgets();

    	$box = $W->VBoxItems();
    	$box->setVerticalSpacing(1, 'em');

    	$lineEdit = $W->LineEdit()->setName('line');
    	$box->addItem(
    		$this->labelledItem('Line edit', $lineEdit)
    	);

    	$checkbox = $W->CheckBox()->setName('checkbox');
    	$box->addItem(
    		$this->labelledItem('Checkbox', $checkbox)
    	);


    	$select = $W->Select()->setName('select');
     	$select->addOption('a1', 'Option 1');
     	$select->addOption('a2', 'Option 2');
      	$box->addItem(
    		$this->labelledItem('Select', $select)
     	);

      	$datePicker = $W->DatePicker()->setName('date');
      	$box->addItem(
      		$this->labelledItem('Date picker', $datePicker)
      	);

      	$colorPicker = $W->ColorPicker()->setName('color');
      	$box->addItem(
      	    $this->labelledItem('Color picker', $colorPicker)
      	);

    	$textEdit = $W->TextEdit()->setName('text');
    	$textEdit->setMandatory('The multiselect is mandatory');
    	$box->addItem(
    		$this->labelledItem('Text edit', $textEdit)
    	);

    	$htmlEdit = $W->SimpleHtmlEdit()->setName('html');
    	$box->addItem(
    		$this->labelledItem('Html edit', $htmlEdit)
    	);


    	$multiSelect = $W->Multiselect();
    	$multiSelect->setName('multiselect');
    	$multiSelect->addOption('1', 'First');
    	$multiSelect->addOption('2', 'Second');

    	$box->addItem(
    	    $this->labelledItem('Multiselect', $multiSelect)
    	);

    	$form = $W->Form('demo-persistent-form', $box);


    	$form->setHiddenValue('tg', bab_rp('tg'));

    	$form->setName('data');

    	$form->addItem(
    	    $W->SubmitButton()
    	       ->setAction($this->proxy()->displayResult())
    	       ->validate(true)
    	);

    	$form->setPersistent(true);

    	return $form;
    }

    /**
     * Line inputs demo.
     *
     * @return Widget_VBoxLayout
     */
    public function form($formDemoId = null)
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $lineEdit = $W->LineEdit();
        $lineEdit->setName('line');
        $lineEdit->setMandatory(true, 'The line edit is mandatory');

        $checkbox = $W->CheckBox()->setName('checkbox');
        $checkbox->setMandatory(true, 'The checkbox is mandatory');

//         $select = $W->Select()->setName('select');
//         $select->addOption('', '');
//         $select->addOption('a1', 'Option 1');
//         $select->addOption('a2', 'Option 2');
//         $select->setMandatory(true, 'The select is mandatory');

//         $datePicker = $W->DatePicker()->setName('date');
//         $datePicker->setMandatory(true, 'The date is mandatory');

//         $colorPicker = $W->ColorPicker()->setName('color');
//         $colorPicker->setMandatory(true, 'The color is mandatory');

//         $textEdit = $W->TextEdit()->setName('text');
//         $textEdit->setMandatory(true, 'The text edit is mandatory');

//         $htmlEdit = $W->SimpleHtmlEdit()->setName('html');
//         $htmlEdit->setMandatory(true, 'The html edit is mandatory');

//         $periodPicker = $W->PeriodPicker()->setName('period');
//         $periodPicker->setMandatory(true, 'The period is mandatory');

        $filePicker1 = $W->FilePicker()->setName('file1');
        $filePicker1->oneFileMode(true);

        $filePicker2 = $W->FilePicker()->setName('file2');
        $filePicker2->oneFileMode(true);

//         $multi = $W->Multiselect();
//         $multi->setName('multi');
//         $multi->setOptions(array('a', 'b', 'c', 'd'));
//         $multi->setMandatory(true, 'The multiselect is mandatory');

//         $multilang = $W->MultilangLineEdit();
//         $multilang->setName('multilang');
//         $multilang->setLanguages(array('en', 'fr', 'es'));
//         $multilang->setMandatory(true, 'The multilang is mandatory');

        $box->addItem(
            $W->Section(
                'Test mandatory',
                $W->VBoxItems(
                    $this->labelledItem('Line edit', $lineEdit),
                    $this->labelledItem('Checkbox', $checkbox),
                    $this->labelledItem('File 1', $filePicker1),
                    $this->labelledItem('File 2', $filePicker2)
//                     $this->labelledItem('Select', $select),
//                     $this->labelledItem('Date picker', $datePicker),
//                     $this->labelledItem('Color picker', $colorPicker),
//                     $this->labelledItem('Html edit', $htmlEdit),
//                     $this->labelledItem('Multiselect', $multi),
//                     $this->labelledItem('Period', $periodPicker),
//                     $this->labelledItem('Multilang', $multilang)
                )->setVerticalSpacing(1, 'em'),
                4
            )->setFoldable(true, false)
            ->setName('data')
        );

        $form = $W->Form('demo-form', $box);
        $form ->addClass('widget-bordered');

        $form->setHiddenValue('tg', bab_rp('tg'));
        $form->setHiddenValue('data[id]', $formDemoId);

        $form->addItem(
            $W->SubmitButton()
                ->validate()
                ->setAction($this->proxy()->displayResult())
        );

        $formDemoSet = new widgetsDemo_FormDemoSet();
        $formDemos = $formDemoSet->select();
        foreach ($formDemos as $formDemo) {
            $form->addItem(
                $W->Link($formDemo->line, $this->proxy()->display($formDemo->id))
            );
        }
        if ($formDemoId) {
            $f = $formDemoSet->get($formDemoId);
            if ($f) {
                $form->setValues($f->getFormOutputValues(), array('data'));
            }
        }

        return $form;
    }

    public function displayResult($data = null)
    {
        var_dump($data);
        $formDemoSet = new widgetsDemo_FormDemoSet();

        $formDemo = $formDemoSet->newRecord();


        $formDemo->setFormInputValues($data);

        $formDemo->save();

        var_dump($formDemo->getValues());

        return true;

    }



    public function slowAjaxForm($itemId = null)
    {
        $W = bab_Widgets();

        $box = $W->FlowItems(
            $this->labelledItem('A', $W->LineEdit()->setName('a')),
            $this->labelledItem('B', $W->LineEdit()->setName('b')),
            $this->labelledItem('C', $W->LineEdit()->setName('c')),
            $this->labelledItem('D', $W->LineEdit()->setName('d'))
        );
        $box->setHorizontalSpacing(1, 'em');

        $form = $W->Form('demo-slowAjaxForm', $box);

        if (isset($itemId)) {
            $form->setId($itemId);
        }

        $form ->addClass('widget-bordered');
        $form->setName('data');

        if (isset($_SESSION[__FILE__ . '/data'])) {
            $form->setValues($_SESSION[__FILE__ . '/data'], array('data'));
        }

        $form->setHiddenValue('tg', bab_rp('tg'));

        $form->setAjaxAction($this->proxy()->slowTreatment(), '', 'change');

        $form->addClass('depends-slowTreatment');
        $form->setReloadAction($this->proxy()->slowAjaxForm($form->getId()));

        return $form;
    }


    public function slowTreatment($data = null)
    {
        sleep(2);
        $_SESSION[__FILE__ . '/data'] = $data;
        $this->addMessage('Slow treatment finished');
        $this->addReloadSelector('.depends-slowTreatment');
        return true;
    }
}
