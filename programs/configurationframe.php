<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


bab_functionality::includefile('Widgets');
Func_Widgets::includePhpClass('Widget_Frame');


class Demo_ConfigurationFrame extends Widget_Frame
{

	public function __construct($id = null)
	{
		$W = $this->widgetFactory();
		parent::__construct($id);
		$this->setLayout($W->VBoxLayout());
	}


	/**
	 * @return Func_Widgets
	 */
	private function widgetFactory()
	{
		return bab_functionality::get('Widgets', false);
	}


	function addTab($id, $label)
	{
		$W = $this->widgetFactory();
	}


	/**
	 * @param string	$id
	 * @param string	$label
	 * @return Widget_Frame
	 */
	function addSection($id, $label)
	{
		$W = $this->widgetFactory();
		$section = $W->Frame($id)->addClass('widget-section');
		$section->setLayout($W->VBoxLayout());
		$sectionLabel = $W->Label($label)->addClass('widget-title');
		$this->addItem(Widget_VBoxLayout()->addItem($sectionLabel)->addItem($section));
		return $section;
	}

}

