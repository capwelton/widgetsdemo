<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';

require_once dirname(__FILE__) . '/functions.php';






/**
 * @return Func_Widgets
 */
function instanciateWidgetFactory()
{
    return bab_functionality::get('Widgets', false);
}


$W = instanciateWidgetFactory();
$W->includeCss();



/**
 * Creates a label and line edit in an hbox layout.
 *
 * @param string	$id
 * @param string	$labelText
 * @param int		$size
 * @return Widget_Frame
 */
function my_LabelEdit($id, $labelText, $size = 20)
{
    global $W;
    $lineEdit = $W->LineEdit($id);

    return $W->Frame()
            ->setLayout($W->VBoxLayout())
            ->addItem($W->Label($labelText)->setAssociatedWidget($lineEdit))
            ->addItem($lineEdit->setName('-')->setSize($size));
}




/**
 * Creates a demonstration listview populated with sample data.
 *
 * @param string	$id
 * @return Widget_ListView
 */
function my_ListView($id)
{
    global $W;
    $listView = $W->ListView($id);

    $listView->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Publics'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Personnels'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'x-office-document.png', 'Document1.doc')->setTitle('Document1.doc|<b>Type</b>: Document Office|<b>Modifi� le</b>: 10/12/2007 15:37'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'audio-x-generic.png', 'Musique.mp3')->setTitle('Musique.mp3|<b>Type</b>: Document audio|<b>Modifi� le</b>: 11/12/2007 12:57'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Document Word'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Document html'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Un autre document Word'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-pdf.png', 'Fichier pdf'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Un troisi�me document html'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Publics'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Personnels'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'x-office-document.png', 'Document1.doc')->setTitle('Document1.doc|<b>Type</b>: Document Office|<b>Modifi� le</b>: 10/12/2007 15:37'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'audio-x-generic.png', 'Musique.mp3')->setTitle('Musique.mp3|<b>Type</b>: Document audio|<b>Modifi� le</b>: 11/12/2007 12:57'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Document Word'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Document html'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Un autre document Word'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-pdf.png', 'Fichier pdf'))
             ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Un troisi�me document html'));
    return $listView;
}



function my_userFrame()
{
    global $W;

    $frame = $W->Frame('user');
    return $frame->addClass('Widget_Resizable')->setLayout($W->VBoxLayout())
            ->setName('user')
            ->addItem(my_LabelEdit('firstname', 'First name:', 40)->setName('firstname'))
            ->addItem(my_LabelEdit('lastname', 'Last name:', 40)->setName('lastname'))
            ->addItem(my_LabelEdit('function', 'Function:', 20)->setName('function'))
            ->addItem($W->VBoxLayout()->addItem($W->Label('Resume:'))->addItem($W->Uploader('resume')->setAcceptedMimeTypes('text/html')->setName('resume')))
            ->addItem(my_LabelEdit('age', 'Age:', 3)->setName('age'));
}


require_once 'configurationframe.php';

/**
 * @link http://wiki.cantico.fr/index.php/D%C3%A9finition_d%27un_th%C3%A8me_d%27ic%C3%B4nes_pour_Ovidentia
 */
function Demo_ListView($id)
{
    global $W;

    $listView = $W->ListView($id)->addClass('widget-configuration-panel');

    if (($I = bab_functionality::get('Icons')) === false) {
        return $listView;
    }

    $I->includeCss();

    $listView->addItem($W->Icon('Configuration du site', Func_Icons::APPS_PREFERENCES_SITE))
             ->addItem($W->Icon('Mail server parameters', Func_Icons::APPS_PREFERENCES_MAIL_SERVER))
             ->addItem($W->Icon('User options', Func_Icons::APPS_PREFERENCES_USER))
             ->addItem($W->Icon('Serveur uploads configuration', Func_Icons::PLACES_FOLDER))
             ->addItem($W->Icon('Date and time format', Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT))
             ->addItem($W->Icon('Calendar and vacation options', Func_Icons::APPS_CALENDAR))
             ->addItem($W->Icon('Authentication configuration', Func_Icons::APPS_PREFERENCES_AUTHENTICATION))
             ->addItem($W->Icon('Registration configuration', Func_Icons::APPS_DIRECTORIES))
             ->addItem($W->Icon('Wysiwyg editor parameters', Func_Icons::APPS_PREFERENCES_WYSIWYG_EDITOR))
             ->addItem($W->Icon('Search engine configuration', Func_Icons::APPS_PREFERENCES_SEARCH_ENGINE))
             ->addItem($W->Icon('Web services', Func_Icons::APPS_PREFERENCES_WEBSERVICES));
             return $listView;
}


function Demo_LabelEdit($labelText, $editSize = null, $id = null)
{
    global $W;

    $color = $W->Create('Widget_Color')->setHueFromString($labelText, 0.80, 0.30);
    $options = Widget_Canvas::Options()->textColor('#'.$color->getHexa());

    $edit = $W->LineEdit($id);
    if (isset($editSize)) {
        $edit->setSize($editSize);
    }
    $label = $W->Label($labelText)->setAssociatedWidget($edit);
    return $W->Frame(null, $W->HBoxLayout())->setCanvasOptions($options)->addItem($label)->addItem($edit);
}


function Demo_CheckboxLabel($labelText, $id = null)
{
    global $W;
    $checkbox = $W->CheckBox($id);
    $label = $W->Label($labelText)->setAssociatedWidget($checkbox);
    return $W->HBoxLayout()->addItem($checkbox)->addItem($label)->addClass('widget-valign-middle');
}




function Demo_demo1()
{
    global $babBody, $W;

    $babBody->title = 'Demo 1: Mixed composite widgets';

    $page = $W->BabPage();


    $configFrame = $W->Frame();

    $configFrame->addItem(
        $W->Section(
            'Section containing icons',
            Demo_ListView('lview')->addClass('icon-left icon-left-48 icon-48x48')
        )->setFoldable(true)
    );


    $configFrame->addItem(
        $W->Section(
            'Section containing tabs',
            $W->Tabs()
                ->addTab(
                        'A tab containing icons',
                        Demo_ListView('lview_t32')->addClass('icon-top icon-top-32 icon-32x32')
                )
                ->addTab(
                    'A calendar',
                    $W->VBoxItems(
                        $W->Frame(null, $W->VBoxItems(
                            $W->FullCalendar()
                                ->setCanvasOptions(Widget_Item::Options()->height(25, 'em'))
                                //							->addGcalFeed('http://www.google.com/calendar/feeds/fr.french%23holiday%40group.v.calendar.google.com/public/basic')
                                //							->addGcalFeed('http://www.google.com/calendar/feeds/fr.usa%23holiday%40group.v.calendar.google.com/public/basic')
                                ->setView(Widget_FullCalendar::VIEW_WORKWEEK)
                            )
                        )->setCanvasOptions(Widget_Item::Options()->height(50, 'em')),


                        $W->Section(
                            'Advanced options',
                            my_userFrame()
                        )->setFoldable(true, true)
                    )
                )


        )->setFoldable(true)
    );

    $configFrame->addItem(
        $W->Section(
            'Calendar',
                $W->FullCalendar()
// 					->addGcalFeed('http://www.google.com/calendar/feeds/fr.french%23holiday%40group.v.calendar.google.com/public/basic')
// 					->addGcalFeed('http://www.google.com/calendar/feeds/fr.usa%23holiday%40group.v.calendar.google.com/public/basic')
                    ->setView(Widget_FullCalendar::VIEW_MONTH)
//					->setCanvasOptions(Widget_Item::Options()->height(20, 'em'))
        )
        ->setFoldable(true)
    );



//	$configFrame2 = new Demo_ConfigurationFrame('conf2');
//
//	$configFrame2->addSection('date-time-format2', 'Date and time format')
//						->addItem(Demo_LabelEdit('long-date-format2', 'Long date format:', 10))
//						->addItem(Demo_LabelEdit('short-date-format2', 'Short date format:', 10))
//						->addItem(Demo_LabelEdit('time-format2', 'Time format:', 6))
//						->addItem(Widget_Label('Short Comment'))
//						->addItem($W->TextEdit()->setValue('Hello wolrd !')->setColumns(40));
//
//	$configFrame2->addSection('fonts2', 'Fonts')
//						->addItem(Demo_LabelEdit('title-font2', 'Title font:', 20))
//						->addItem(Demo_LabelEdit('body-font2', 'Body font:', 20))
//						->addItem(Demo_CheckboxLabel('bold2', 'Bold'))
//						->addItem(Demo_CheckboxLabel('italic2', 'Italic'));

    $page->addItem($configFrame);

    $page->displayHtml();
}




/**
 * render a labeled string
 *
 * @param	string | Widget_Item	$label
 * @param	string | Widget_Item	$value
 *
 * @return Widget_Item
 */
function labelStr($label, $value)
{
    global $W;

    if (!($label instanceOf Widget_Item)) {
        $label = $W->Label($label);
    }

    if (!($value instanceOf Widget_Displayable_Interface)) {
        $value = $W->Label($value);
    }else{
        $value->setAssociatedLabel($label);
    }

    return $W->VBoxItems(
        $label->colon(false),
        $value
    )->setVerticalAlign('middle');
}


function art_pub()
{
    $I = bab_functionality::get('Icons');
    $I->includeCss();
    global $babBody, $W;

    $babBody->title = 'Publication des articles';

    $page = $W->BabPage();


    $LeftFrame = $W->VBoxLayout()->setVerticalSpacing(10,'px');

    $LeftFrame->addItem($W->Html('
        <style type="text/css">
            #title, #intro, #corps, #modify{
                width: 100%;
            }
            #title-label{
                font-weight: bold;
            }
            #textEdit_modify{
                width: 100%;
            }
            #textEdit_modify{
                border: 2px solid #aaa !important;
            }
            #global-article-page{
                margin: 20px;
                margin-top: 0;
            }
            .nowrap{
                white-space: nowrap;
            }
        </style>
    '));
    $LeftFrame->addItem(
        $W->Section(
            $tempLab = $W->Label('Titre'),
            $W->Frame()->addItem(
                $W->LineEdit('title')->setAssociatedLabel($tempLab)->setMandatory(true,'Le champs titre est obligatoire')->setName('title')
            )
        )->setFoldable(false)
    );

    $LeftFrame->addItem(
        $W->Section(
            $tempLab = $W->Label('Introduction'),
            $W->Frame('intro')->addItem(
                $W->BabHtmlEdit()->setAssociatedLabel($tempLab)->setMandatory(true,'Le corps est obligatoire')->setName('intro')
            )
        )->setFoldable(true)
    );

    $LeftFrame->addItem(
        $W->Section(
            'Corps',
            $W->Frame('corps')->addItem(
                $W->BabHtmlEdit()->setName('body')
            )
        )->setFoldable(true)
    );

    $LeftFrame->addItem(
        $W->Section(
            'Raisons de modifications',
            $W->Frame('modify')->addItem(
                $W->TextEdit('textEdit_modify')->setName('modify')
            )
        )->setFoldable(true, true)
    );

    $LeftFrame->addItem(
        $W->HBoxItems(
            $W->SubmitButton()->setLabel('Annuler')->setName('cancel'),
            $W->SubmitButton()->validate(true)->setLabel('Enregistrer en brouillon')->setName('draft'),
            $W->SubmitButton()->validate(true)->setLabel('Previsualisation')->setName('see'),
            $W->SubmitButton()->validate(true)->setLabel('Soumettre')->setName('submit')
        )->setHorizontalSpacing(5,'px')
    );

    $RightFrame = $W->VBoxLayout()->setVerticalSpacing(10,'px');

    $RightFrame->addItem(
        labelStr(
            'Theme de l\'article',
            $W->Select()
                ->setValue('1')
                ->setName('theme')
                ->addOption('','')
                ->addOption('1','Theme d\'article n1')
                ->addOption('2','Theme d\'article n2')
                ->addOption('3','Theme d\'article n3')
        )
    );
    $RightFrame->addItem(
        labelStr(
            'Date de soumission',
            $W->DatePicker()->setName('submit_date')->setValue(date('d-m-Y'))
        )
    );
    $RightFrame->addItem(
        labelStr(
            'Date de publication',
            $W->DatePicker()->setName('publication_date')->setValue(date('d-m-Y'))
        )
    );
    $RightFrame->addItem(
        labelStr(
            'Date d\'archivage',
            $W->DatePicker()->setName('archived_date')
        )
    );
    $RightFrame->addItem(
        $W->HBoxItems(
            $tempCheck = $W->CheckBox()->setName('public_page')->setCheckedValue('1'),
            $W->Label("Proposer pour la page d'acceuil publique")->setAssociatedWidget($tempCheck)->addClass('nowrap')
        )->setVerticalSpacing(5, 'px')->setVerticalAlign('middle')
    );
    $RightFrame->addItem(
        $W->HBoxItems(
            $tempCheck = $W->CheckBox()->setName('private_page')->setCheckedValue('1'),
            $W->Label("Proposer pour la page d'acceuil prive")->setAssociatedWidget($tempCheck)->addClass('nowrap')
        )->setVerticalSpacing(5, 'px')->setVerticalAlign('middle')
    );
    $RightFrame->addItem(
        $W->HBoxItems(
            $tempCheck = $W->CheckBox()->setName('notify_reader')->setCheckedValue('1'),
            $W->Label("Notifier les utilisateurs une fois l'article publie")->setAssociatedWidget($tempCheck)->addClass('nowrap')
        )->setVerticalSpacing(5, 'px')->setVerticalAlign('middle')
    );

    $RightFrame->addItem(
        labelStr(
            "Langue de l'article",
            $W->Select()
                ->setValue('fr')
                ->setName('langue')
                ->addOption('*','*')
                ->addOption('fr','fr')
                ->addOption('de','de')
                ->addOption('en','en')
                ->addOption('nl','nl')
                ->addOption('nl-be','nl-be')
        )
    );

    $RightFrame->addItem(
        labelStr(
            "Mots clef",
            $W->LineEdit()->setName('tags')->setMandatory(true,'Les mots clef sont obligatoire')
        )
    );

    $RightFrame->addItem(
        labelStr(
            "Restriction d'acces",
            $W->Select()
                ->setName('langue')
                ->addOption('0','Pas de restriction')
                ->addOption('1','Groupes')
        )
    );

    $RightFrame->addItem(
        labelStr(
            "Avec l'operateur",
            $W->Select()
                ->setName('langue')
                ->addOption('2','Ou')
                ->addOption('1','Et')
        )
    );

    $RightFrame->addItem(
        $W->FilePicker()->oneFileMode(true)->setTitle('Ajouter une image')->setName('image')
    );

    $RightFrame->addItem(
        $W->FilePicker()->setTitle('Ajouter un fichier')->setName('files')
    );

    $globalFrame = $W->HboxItems(
        $LeftFrame->setSizePolicy(Widget_SizePolicy::MAXIMUM),
        $RightFrame->setSizePolicy(Widget_SizePolicy::MINIMUM)
    )->setHorizontalSpacing(30, 'px')->setId('global-article-page');

    $FormArticle = $W->Form('article-form',$globalFrame)
        ->setHiddenValue('tg', 'trucmuche')
        ->setHiddenValue('idx', 'action');

    $page->addItem($FormArticle);

    $page->displayHtml();
}







/**
 * @param unknown_type $steps
 * @param unknown_type $width
 * @param unknown_type $method
 * @param unknown_type $c1
 * @return Widget_Html
 */
function Demo_colorFrame($steps, $width, $method, $c1)
{
    global $babBody, $W;

    $color = $W->create('Widget_Color');

    $stepWidth = $width / $steps;
    $html = '';
    for ($v = 0; $v < 1; $v += 1/$steps) {
        $html .= '<div style="clear: left; float: left; width: '. ($width / 8). 'px; height: 1px"></div>';
        for ($u = 0; $u < 1; $u += 1/$steps) {

            $color->$method($c1, $u, $v);
            $html .= '<div style="float: left; width: ' . $stepWidth . 'px; height: ' . $stepWidth . 'px; padding: 0; color: #fff; background-color:#'. $color->getHexa().'">&nbsp;</div>';
        }
    }
    return $W->create('Widget_Html', $html);
}


function Demo_randomColorFrame($steps, $width, $L)
{
    global $babBody, $W;

    $color = $W->create('Widget_Color');

    $stepWidth = $width / $steps;
    $html = '';
    for ($v = 0; $v < 1; $v += 1/$steps) {
        $html .= '<div style="clear: left; float: left; width: '. ($width / 8). 'px; height: 1px"></div>';
        for ($u = 0; $u < 1; $u += 1/$steps) {

            $color->setHueFromString(uniqid(), 1, $L);
            $html .= '<div style="float: left; width: ' . $stepWidth . 'px; height: ' . $stepWidth . 'px; padding: 0; color: #fff; background-color:#'. $color->getHexa().'">&nbsp;</div>';
        }
    }
    return $W->create('Widget_Html', $html);
}








function Demo_color()
{
    global $babBody, $W;
    $canvas = $W->HtmlCanvas();

    $babBody->setTitle('Color Demo: using Widget_Color');

    $color = $W->create('Widget_Color');

    $steps = 24;
    $width = 120;

    $stepWidth = $width / $steps;

    $configFrame = new Demo_ConfigurationFrame('color');

    $name = $W->LineEdit();

    $configFrame->addSection('widget_test', 'Color picker')
                        ->addItem($W->HBoxItems($W->Label('Test')->colon(), $name, $W->ColorPicker()->setAssociatedLineEdit($name)))
                        ->addItem($W->HBoxItems($W->Label('YUV')->colon(), $W->ColorPicker()->setColorSpace('YUV')))
                        ->addItem($W->HBoxItems($W->Label('YUV')->colon(), $W->ColorPicker()->setColorSpace('YUV')->setRules('V','Y','U')))
                        ->addItem($W->HBoxItems($W->Label('YUV')->colon(), $W->ColorPicker()->setColorSpace('YUV')->setRules('V','U','Y')->setSliderValue(0.95)->slider(false)))
                        ->addItem($W->HBoxItems($W->Label('HSL')->colon(), $W->ColorPicker()->setColorSpace('HSL')->setRules('H','L','S')->setSliderValue(0.8)))
                        ->addItem($W->HBoxItems($W->Label('RGB')->colon(), $W->ColorPicker()->setColorSpace('RGB')->slider(false)))
                        ->addItem($W->HBoxItems($W->Label('RGB')->colon(), $W->ColorPicker()->setColorSpace('RGB')->setRules('G','B','R')->setSliderValue(0)))
                        ;


    $frame = $W->Frame()->setLayout($W->FlowLayout());
    for ($y = 0.75; $y <= 1; $y += 0.2) {
        $colorWidget = Demo_colorFrame($steps, $width, 'setYUV', $y);
        $frame->addItem($W->VBoxLayout()
                        ->addItem($W->Label('Y ' . $y))
                        ->addItem($colorWidget)
                        );
    }
    $configFrame->addSection('yuv', 'YUV Color Space')
                        ->addItem($frame);

    $frame = $W->Frame()->setLayout($W->FlowLayout());
    for ($r = 0.25; $r <= 1; $r += 0.4) {
        $colorWidget = Demo_colorFrame($steps, $width, 'setRGB', $r);
        $frame->addItem($W->VBoxLayout()
                        ->addItem($W->Label('R ' . $r))
                        ->addItem($colorWidget)
                        );
    }
    $configFrame->addSection('rgb', 'RGB Color Space')
                        ->addItem($frame);


    $frame = $W->Frame()->setLayout($W->FlowLayout());
    for ($h = 0.25; $h <= 1; $h += 0.5) {
        $colorWidget = Demo_colorFrame($steps, $width, 'setHSL', $h);
        $frame->addItem($W->VBoxLayout()
                        ->addItem($W->Label('H ' . $h))
                        ->addItem($colorWidget)
                        );
    }
    $configFrame->addSection('hsl', 'HSL Color Space')
                        ->addItem($frame);




    $frame = $W->Frame()->setLayout($W->FlowLayout());
    for ($L = 0.35; $L <= 1; $L += 0.3) {
        $colorWidget = Demo_randomColorFrame($steps, $width, $L);
        $frame->addItem($W->VBoxLayout()
                        ->addItem($W->Label('L ' . $L))
                        ->addItem($colorWidget)
                        );
    }
    $configFrame->addSection('rand', 'setHueFromString(uniqid(), 1, L)')
                        ->addItem($frame);



    $babBody->babEcho($configFrame->display($canvas));

}




function Demo_Radiomenu() {
    global $W;

    bab_functionality::includefile('Icons');
    $iconReflector = new ReflectionClass('Func_Icons');
    $constants = array_flip($iconReflector->getConstants());

    bab_Sort::natcasesort($constants);

    $radiomenu = $W->RadioMenu()->addClass('icon-left icon-left-32 icon-32x32')->width(60, 'px');

    foreach($constants as $key => $c) {
        $radiomenu->addOption($key, $W->Icon($c, eval('return Func_Icons::'.$c.';')));
    }

    return $W->FlowItems($W->Label('Icon')->colon(),$radiomenu)->setSpacing(.5,'em')->setVerticalAlign('middle');
}



function Demo_business() {

    global $W;

    $layout = $W->create('Widget_BusinessAppLayout');
    $page = $W->create('Widget_BusinessApplicationPage',null, $layout);
    $page->setJavascriptToolkit('jquery');


    $page->setTitle('Business Application test page');

    $page->setPortalLink('?');




    if (bab_rp('theme')) {
        $page->addStyleSheet('http://www.filamentgroup.com/examples/RollAUI/jquery-ui-themeroller.'.bab_rp('theme').'.css');
    } else {
        $page->addStyleSheet(bab_getAddonInfosInstance('widgets')->getStylePath().'default/theme.css');
    }

    $options = array(

        'smoothness' => 'smoothness',
        'excitebike' => 'excitebike',
        'southstreet' => 'southstreet',
        'blacktie' => 'blacktie',
        'creamsicle' => 'creamsicle',
        'corpcool' => 'corpcool',
        'mintchoco' => 'mintchoco',
        'dustyshelf' => 'dustyshelf',
        'dotluv' => 'dotluv',
        'uisite' => 'uisite',
        'cupertino' => 'cupertino'
    );

    $section = $page->createSection();
    $section->addItem(
        $W->Form(null, $W->VBoxLayout())
            ->setReadOnly()
            ->addItem($W->create('Widget_Select', null)->setName('theme')->setOptions($options))
            ->addItem($W->SubmitButton()->setLabel('Apply'))
            ->setSelfPageHiddenFields()
            ->setValues($_GET)
    );







    $page->addRailItem('test1', 'test1', '?')->addRailItem('test2', 'test2', '?');

    $page->addItemMenu('onglet1', 'Onglet 1', '?')->addItemMenu('onglet2', 'Onglet 2', '?')->addItemMenu('onglet3', 'Onglet 3', '?');
    $page->setCurrentItemMenu('onglet2');


    $div = $page->createFrame();


    // external widgets

    $upload = @bab_functionality::get('FileUploader/ImageUploader');
    if (false !== $upload) {
        $upload->setFileUid('WidgetDemo_business');

        $multipleup = @bab_functionality::get('FileListUploader');
        if (false !== $multipleup) {
            $multipleup->setListUid('WidgetDemo_business_multipleup');
            $div->addItem(
                $W->HBoxItems($upload->getWidget(), $multipleup->getWidget() )
            );
        }
    }



    // multifield demo
    // use ->setJavascriptToolkit('jquery') on widget page

    $postalcode = $W->SuggestPostalCode()->setName('postalcode');
    $placename = $W->SuggestPlaceName()->setName('city');
    $pays = $W->SuggestCountry()->setName('country');



    $div
    ->addItem(

        $W->HBoxItems(

            $W->Pair(
                $W->Label('LineEdit'),
                $W->MultiField()
                ->setName('multifield_LineEdit')
                ->addItem($W->LineEdit()->setName('0'))
                ->addItem($W->LineEdit()->setName('1'))
                ->addItem($W->LineEdit()->setName('2'))
            ),

            $W->Pair(
                $W->Label('Select'),
                $W->MultiField()
                ->setName('multifield_Select')
                ->addItem($W->Select()->setOptions(array('1' => 'one', '2' => 'two'))->setName('0'))
            ),

            $W->Pair(
                $W->Label('Code postal'),
                $postalcode->setRelatedCountry($pays)->setRelatedPlaceName($placename)
            ),

            $W->Pair(
                $W->Label('Ville'),
                $placename->setRelatedCountry($pays)->setRelatedPostalCode($postalcode)
            ),

            $W->Pair(
                $W->Label('Pays'),
                $pays
            )

        )
    );




    $section = $page->createSection('Demo liens');
    $section->addItem($page->html_links(array(
        array(
            'title' => 'link 1',
            'url' => '?'
        ),
        array(
            'title' => 'link 2',
            'url' => '?',
            'classname' => 'selected'
        ),
        array(
            'title' => 'link 3',
            'url' => '?'
        ),
        array(
            'title' => 'sublink 3',
            'url' => '?',
            'classname' => 'sublink'
        )
    )));



    $section = $page->createSection('Demo paires');

    $section->addItem(
        $W->create('Widget_Pair')
        ->setFirst($W->create('Widget_Label', 'titre'))
        ->setSecond($W->create('Widget_Label', 'valeur'))
    );


    $section->addItem(
        $W->create('Widget_Frame', null, $W->create('Widget_PairVBoxLayout', null, 'liste de test'))
        ->addItem(
            $W->create('Widget_Pair')
            ->setFirst($W->create('Widget_Label', 'titre 1'))
            ->setSecond($W->create('Widget_Label', 'valeur 1'))
        )
        ->addItem(
            $W->create('Widget_Pair')
            ->setFirst($W->create('Widget_Label', 'titre 2'))
            ->setSecond($W->create('Widget_Label', 'valeur 2'))
        )
        ->addItem(
            $W->create('Widget_Label', 'tout seul')
        )
        ->addItem(
            $W->create('Widget_Pair')
            ->setFirst($W->create('Widget_Label', 'titre 3'))
            ->setSecond($W->create('Widget_Label', 'valeur 3'))
        )
    );

    $section->addItem(
        $page->html_trList(array(

            array(
                'title' => 'titre 1',
                'value' => 'valeur 1'
            ),
            array(
                'title' => 'titre 2',
                'value' => 'valeur 2',
                'classname' => 'money'
            )

        ), $caption = 'liste de test 2')
    );


    global $babDB;
    $res = $babDB->db_query('SELECT * FROM bab_upgrade_messages');


    $div = $page->createFrame('List demo')->addItem(
        $W->create('Widget_BusinessApplicationList')->sqlHelper($res, array('bab_upgrade_messages'))
    );






    bab_debug($page->dump());

    $page->displayHtml();

}









function Demo_drilldown()
{
    global $babBody, $W;
    /* @var $W Func_Widgets */
    $canvas = $W->HtmlCanvas();

    $babBody->setTitle('Drilldown menu demo');

    $configFrame = new Demo_ConfigurationFrame();

    $widget = $W->TopicPicker();
    $widget->setName('Topic');

    $configFrame->addSection('widget_test', 'Test')

                        ->addItem($W->HBoxItems($W->Label('Menu')->colon(), $widget)->setVerticalAlign('middle'))
                        ->addItem($W->HBoxItems($W->Label('Group')->colon(), $W->GroupPicker()->setName('group'))->setVerticalAlign('middle'))
                        ->addItem($W->VBoxItems($W->Label('ACL')->colon(),  $W->Acl()->setTitle('Qui peut voir la chose?')->setMaxHeight(null)->setName('MyAcl'))->setVerticalAlign('middle'))

                        ->getLayout()->setVerticalSpacing(2,'em')
                        ;




    $babBody->babEcho($configFrame->display($canvas));
}



function Demo_imagecropper()
{
    global $babBody, $W;
    /* @var $W Func_Widgets */
    $canvas = $W->HtmlCanvas();

    $babBody->setTitle('Image Cropper demo');

    $configFrame = new Demo_ConfigurationFrame();

    $widget = $W->ImageCropper();
    $widget->setName('test');

    $T = bab_functionality::get('Thumbnailer');
    /* @var $T Func_Thumbnailer */

    $T->setSourceBinary(file_get_contents('http://www.momesdazur.com/images2/origami%20fleurs.jpg'), date('Y-m-d'));
    $widget->setUrl($T->getThumbnail(9999, 9999));
    $widget->setRatio(16/9);
    $widget->setMinSize(1280, 720);
    //$widget->setValue(266, 235, 460, 417);

    $configFrame->addSection('widget_test', 'Test')
        ->addItem($W->HBoxItems($widget)->setVerticalAlign('middle'))
    ;

    $T->setSourceBinary(file_get_contents('http://www.photo-pixel.eu/paysage-feerique/paysage-feerique.jpg'), date('Y-m-d'));
    $widget = $W->ImageCropper();
    $widget->setName('test2');
    $widget->setUrl($T->getThumbnail(9999, 9999));
    $widget->setValue(166, 235, 250, 200);

    $configFrame->addSection('widget_test2', 'Test2')
        ->addItem($W->HBoxItems($widget)->setVerticalAlign('middle'))
    ;




    $babBody->babEcho($configFrame->display($canvas));
}




$idx = bab_rp('idx', 'demo1');

$W = Demo_widgetFactory();

$page = $W->BabPage('widgets_demo', $W->VBoxLayout());


switch ($idx)
{
    case 'demo1':
        Demo_demo1();
        break;

    case 'art_pub':
        Art_pub();
        break;

    case 'icons':
        require_once dirname(__FILE__) . '/icons.php';
        $page->setTitle('Demo: using icon themes');
        $page->addItem(Demo_Radiomenu());
        $f = Demo_icons();
        $page->addItem($f);
        break;

    case 'accordions':
        require_once dirname(__FILE__) . '/accordions.php';
        $page->setTitle('Demo: accordions');
        $f = Demo_accordions();
        $page->addItem($f);
        break;

    case 'color_demo':
        Demo_color();
        break;

    case 'business':
        Demo_business();
        break;

    case 'drilldown':
        Demo_drilldown();
        break;

    case 'imagecropper':
        Demo_imagecropper();
        break;
}

require_once dirname(__FILE__) . '/controller.class.php';

$Ctrl = new widgetsDemo_Controller();

$page->addItemMenu('demo1', 'Demo 1', $GLOBALS['babAddonUrl'].'demo&idx=demo1');
$page->addItemMenu('icons', 'Icons demo', $GLOBALS['babAddonUrl'].'demo&idx=icons');
$page->addItemMenu('accordions', 'Accordions demo', $GLOBALS['babAddonUrl'].'demo&idx=accordions');
$page->addItemMenu('color_demo', 'Color Demo', $GLOBALS['babAddonUrl'].'demo&idx=color_demo');
//$page->addItemMenu('art_pub', 'Article Publication', $GLOBALS['babAddonUrl'].'demo&idx=art_pub');
//$page->addItemMenu('business', 'Business', $GLOBALS['babAddonUrl'].'demo&idx=business');
$page->addItemMenu('drilldown', 'Drilldown menu', $GLOBALS['babAddonUrl'].'demo&idx=drilldown');
$page->addItemMenu('imagecropper', 'Image cropper', $GLOBALS['babAddonUrl'].'demo&idx=imagecropper');

$page->addItemMenu('inputDemo', 'Input', $Ctrl->InputDemo()->display()->url());
$page->addItemMenu('formDemo', 'Form', $Ctrl->FormDemo()->display()->url());
$page->addItemMenu('orgChartDemo', 'Org charts', $Ctrl->OrgChartDemo()->display()->url());
$page->setCurrentItemMenu($idx);

$page->displayHtml();
