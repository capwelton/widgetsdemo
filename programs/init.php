<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';




function widgetsDemo_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';


    $addon = bab_getAddonInfosInstance('widgetsDemo');

    $addon->removeAllEventListeners();

    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'widgetsDemo_onSiteMapItems', 'init.php');

    require_once dirname(__FILE__) . '/set/sample.class.php';
    require_once dirname(__FILE__) . '/set/formdemo.class.php';

    $sampleSet = new widgetsDemo_SampleSet();
    $formDemoSet = new widgetsDemo_FormDemoSet();

    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet($sampleSet);
    $synchronize->addOrmSet($formDemoSet);
    $synchronize->updateDatabase();

    $sampleSet->delete();
    $formDemoSet->delete();

    global $babDB;

    bab_setTimeLimit(60);

    $babDB->db_query('START TRANSACTION');

    for ($i = 0; $i < 10000; $i++) {
    	if ($i % 1000 == 0) echo $i . ' ';
    	$sample = $sampleSet->newRecord();
    	$sample->name = 'N. ' . $i;
    	$sample->quantity = rand(20, 100000);
    	$sample->description = 'This is number ' . $i;
    	$sample->save();
    }

    $babDB->db_query('COMMIT');

    return true;
}



/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function widgetsDemo_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';

    $item = $event->createItem('widgetsDemo_root');
    $item->setLabel(widgetsDemo_translate('Widgets demo'));
    $item->setLink(widgetsDemo_Controller()->Main()->display()->url());
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    $item->addIconClassname('apps-addon-widgetsdemo');
    $event->addFunction($item);
}
